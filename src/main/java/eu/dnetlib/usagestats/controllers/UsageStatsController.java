package eu.dnetlib.usagestats.controllers;

import eu.dnetlib.usagestats.portal.CountryRepositories;
import eu.dnetlib.usagestats.portal.CountryUsageStats;
import eu.dnetlib.usagestats.portal.CountryUsageStatsAll;
import eu.dnetlib.usagestats.portal.MonthlyUsageStats;
import eu.dnetlib.usagestats.portal.TotalStats;
import eu.dnetlib.usagestats.portal.TotalStatsReposViewsDownloads;
import eu.dnetlib.usagestats.portal.UsageStats;

import eu.dnetlib.usagestats.services.UsageStatsService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(methods = RequestMethod.GET, origins = "*")
public class UsageStatsController {

    private final UsageStatsService usageStatsService;

    private final Logger log = Logger.getLogger(this.getClass());

    public UsageStatsController(UsageStatsService usageStatsService) {
        this.usageStatsService = usageStatsService;
    }

    @RequestMapping(value = "/datasources/{datasourceId}/clicks")
    public UsageStats getDatasourceClicks(@PathVariable(value = "datasourceId") String datasourceId) {
        log.info("stats request for datasource: " + datasourceId);
        return usageStatsService.getDatasourceClicks(datasourceId);
    }

    @RequestMapping(value = "/projects/{projectId}/clicks")
    public UsageStats getProjectClicks(@PathVariable(value = "projectId") String projectId) {
        log.info("stats request for project: " + projectId);
        return usageStatsService.getProjectClicks(projectId);
    }

    @RequestMapping(value = "/monthlyusagestats")
    public List<MonthlyUsageStats> getMonthlyUsageStats() {
        log.info("stats request for months");
        return usageStatsService.getMonthlyUsageStats();
    }

    @RequestMapping(value = "/countryusagestats")
    public CountryUsageStatsAll getCountryUsageStatsAll() {
        log.info("stats request for countries");
        return usageStatsService.getCountryUsageStatsAll();
    }
    @RequestMapping(value = "/countryusagestats/{country}")
    public CountryUsageStats getCountryUsageStats(@PathVariable(value = "country") String country) {
        log.info("stats request for country "+country);
        return usageStatsService.getCountryUsageStats(country);
    }

    @RequestMapping(value = "/countryrepositories")
    public List<CountryRepositories> getCountryRepositories() {
        log.info("stats request for countries/repos");
        return usageStatsService.getCountryRepositories();
    }


    @RequestMapping(value = "/totals")
    public TotalStats getTotalStats() {
        log.info("total stats request");
        return usageStatsService.getTotalStats();
    }

    @RequestMapping(value = "/monthlyusagestats/{datasourceId}")
    public List<MonthlyUsageStats> getMonthlyUsageStatsForRepo(@PathVariable(value = "datasourceId") String datasourceId) {
        log.info("stats request for datasource: " + datasourceId);
        return usageStatsService.getMonthlyUsageStatsForRepo(datasourceId);
    }

    
    /*
    @RequestMapping(value = "/organizations/{organizationId}/clicks")
    public UsageStats getOrganizationClicks(@PathVariable(value = "organizationId") String organizationId) {
        log.info("stats request for organization: " + organizationId);
        return usageStatsService.getOrganizationClicks(organizationId);
    }
    */

    @RequestMapping(value = "/results/{resultId}/clicks")
    public UsageStats getResultClicks(@PathVariable(value = "resultId") String resultId) {
        log.info("stats request for result: " + resultId);
        return usageStatsService.getResultClicks(resultId);
    }

    @RequestMapping(value = "/resultspid", method = RequestMethod.GET)
    public UsageStats getResultClicksByPID(@RequestParam(value = "doi", defaultValue = "") String doi) {
        log.info("stats request for pid: " + doi);
        return usageStatsService.getResultClicksByPID(doi);
    }

    @RequestMapping(value = "/allmetrics")
    public TotalStatsReposViewsDownloads getTotalStatsReposViewsDownloads() {
        log.info("total stats request");
        return usageStatsService.getTotalStatsReposViewsDownloads();
    }

}
