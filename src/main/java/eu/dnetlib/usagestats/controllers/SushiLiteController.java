package eu.dnetlib.usagestats.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.usagestats.services.SushiLiteService;
import eu.dnetlib.usagestats.sushilite.domain.SUSHI_Error_Model;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by D.Pierrakos
 */
@RestController
class SushiLiteController {

    private final Logger log = Logger.getLogger(this.getClass());

    private final SushiLiteService sushiLiteService;
    @Value("${download.folder}")
    private String download_folder;

    public SushiLiteController(SushiLiteService sushiLiteService) {
        this.sushiLiteService = sushiLiteService;
    }

    @RequestMapping(value = "/sushilite/r5/status", method = RequestMethod.GET)
    public String getReportStatus() {
        log.info("COUNTER Report status request ");
        return sushiLiteService.displayReportStatus();
    }

    @RequestMapping(value = "/sushilite/r5/reports", method = RequestMethod.GET)
    public String getReportSupported() {
        log.info("COUNTER Supported Reports request ");
        return sushiLiteService.displayReportsSupported();
    }

    @RequestMapping(value = "/sushilite/r5/members", method = RequestMethod.GET)
    public String getMembers() {
        log.info("COUNTER Members request ");
        return sushiLiteService.displayConsortiumMemberList();
    }

    @RequestMapping(value = "/sushilite/r5/reports/pr_p1", method = RequestMethod.GET)
    public String getReportPR_P1(
            @RequestParam(value = "RepositoryIdentifier", defaultValue = "") String repositoryIdentifier,
            @RequestParam(value = "RequestorID", defaultValue = "anonymous") String requestorId,
            @RequestParam(value = "BeginDate", defaultValue = "") String beginDate,
            @RequestParam(value = "EndDate", defaultValue = "") String endDate) {
        log.info("COUNTER PR_P1 Report request for repository " + repositoryIdentifier);
        return sushiLiteService.displayReportPR_P1(requestorId, repositoryIdentifier, beginDate, endDate);
    }

    @RequestMapping(value = "/sushilite/r5/reports/ir", method = RequestMethod.GET)
    public ResponseEntity getReportΙR(
            @RequestParam(value = "RepositoryIdentifier", defaultValue = "") String repositoryIdentifier,
            @RequestParam(value = "ItemIdentifier", defaultValue = "") String itemIdentifier,
            @RequestParam(value = "RequestorID", defaultValue = "anonymous") String requestorId,
            @RequestParam(value = "BeginDate", defaultValue = "") String beginDate,
            @RequestParam(value = "EndDate", defaultValue = "") String endDate,
            @RequestParam(value = "MetricType", defaultValue = "") List<String> metricType,
            @RequestParam(value = "DataType", defaultValue = "") String dataType,
            @RequestParam(value = "Granularity", defaultValue = "Monthly") String granularity) {
        log.info("COUNTER ΙR Report request for repository " + repositoryIdentifier + " and for item " + itemIdentifier);
        class BuildReportWithTimeout implements Callable<ResponseEntity> {

            @Override
            public ResponseEntity call() throws Exception {
                String report = sushiLiteService.displayReportIR(requestorId, repositoryIdentifier, itemIdentifier, beginDate, endDate, metricType, dataType, granularity);
                if (report.indexOf(".zip") < 0) {
                    return new ResponseEntity<>(report, HttpStatus.OK);
                } else {
                    ObjectMapper objectMapper = new ObjectMapper();
                    //String compressedOutput = "<pre> {\"Report\":\"IR\", \"Description\":\"Compressed Report Due to large number of records\", \"URL To Download Report from: \":\"" + report + "\"} </pre>";
                    SUSHI_Error_Model errorModel = new SUSHI_Error_Model("100000", "Notice", "Compressed IR report due to a large number of records",
                            null, report);
                    String compressedOutput = "<pre>" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(errorModel) + "</pre>";
                    return new ResponseEntity<>(compressedOutput, HttpStatus.OK);
                }
            }
        }
        final ExecutorService executor = Executors.newSingleThreadExecutor();
        final Future<ResponseEntity> future = executor.submit(new BuildReportWithTimeout());

        try {
            return future.get(2, TimeUnit.MINUTES);
        } catch (InterruptedException ie) {
            String compressedOutput = "<pre> {\"Report\":\"IR\", \"Description\":\"Large Report Timeout. Please contact usagecounts@openaire.eu for requests" + "\"} </pre>";
            return new ResponseEntity<>(compressedOutput, HttpStatus.OK);
        } catch (ExecutionException ee) {
            /* Handle the error. Or ignore it. */
        } catch (TimeoutException te) {
            String compressedOutput = "<pre> {\"Report\":\"IR\", \"Description\":\"Large Report Timeout. Please contact usagecounts@openaire.eu for requests" + "\"} </pre>";
            return new ResponseEntity<>(compressedOutput, HttpStatus.OK);

        }
        if (!executor.isTerminated()) {
            executor.shutdownNow();
        }

        return null;
//        String report = sushiLiteService.displayReportIR(requestorId, repositoryIdentifier, itemIdentifier, beginDate, endDate, metricType, dataType, granularity);
//
//        if (report.indexOf(".zip") < 0) {
//            return new ResponseEntity<>(report, HttpStatus.OK);
//        } else {
//
//            String compressedOutput = "<pre> {\"Report\":\"IR\", \"Description\":\"Compressed Report Due to large number of records\", \"URL To Download Report from: \":\"" + report + "\"} </pre>";
//            return new ResponseEntity<>(compressedOutput, HttpStatus.OK);
//        }
    }

    @RequestMapping(value = "/sushilite/r5/reports/dsr", method = RequestMethod.GET)
    public ResponseEntity getReportDSR(
            @RequestParam(value = "RepositoryIdentifier", defaultValue = "") String repositoryIdentifier,
            @RequestParam(value = "DatasetIdentifier", defaultValue = "") String itemIdentifier,
            @RequestParam(value = "RequestorID", defaultValue = "anonymous") String requestorId,
            @RequestParam(value = "BeginDate", defaultValue = "") String beginDate,
            @RequestParam(value = "EndDate", defaultValue = "") String endDate,
            @RequestParam(value = "MetricType", defaultValue = "") List<String> metricType,
            @RequestParam(value = "Granularity", defaultValue = "Monthly") String granularity) throws Exception {
        log.info("COUNTER DSR Report request for repository " + repositoryIdentifier + " and for item " + itemIdentifier);

        class BuildReportWithTimeout implements Callable<ResponseEntity> {

            @Override
            public ResponseEntity call() throws Exception {
                String report = sushiLiteService.displayReportDSR(requestorId, repositoryIdentifier, itemIdentifier, beginDate, endDate, metricType, granularity);
                if (report.indexOf(".zip") < 0) {
                    return new ResponseEntity<>(report, HttpStatus.OK);
                } else {

                    String compressedOutput = "<pre> {\"Report\":\"DSR\", \"Description\":\"Compressed Report Due to large number of records\", \"URL To Download Report from: \":\"" + report + "\"} </pre>";
                    return new ResponseEntity<>(compressedOutput, HttpStatus.OK);
                }
            }
        }
        final ExecutorService executor = Executors.newSingleThreadExecutor();
        final Future<ResponseEntity> future = executor.submit(new BuildReportWithTimeout());

        try {
            return future.get(2, TimeUnit.MINUTES);
        } catch (InterruptedException ie) {
            String compressedOutput = "<pre> {\"Report\":\"DSR\", \"Description\":\"Large Report Timeout. Please contact usagecounts@openaire.eu for requests" + "\"} </pre>";
            return new ResponseEntity<>(compressedOutput, HttpStatus.OK);
        } catch (ExecutionException ee) {
            /* Handle the error. Or ignore it. */
        } catch (TimeoutException te) {
            String compressedOutput = "<pre> {\"Report\":\"DSR\", \"Description\":\"Large Report Timeout. Please contact usagecounts@openaire.eu for requests" + "\"} </pre>";
            return new ResponseEntity<>(compressedOutput, HttpStatus.OK);

        }
        if (!executor.isTerminated()) {
            executor.shutdownNow();
        }

        return null;
//        String report = sushiLiteService.displayReportDSR(requestorId, repositoryIdentifier, itemIdentifier, beginDate, endDate, metricType, granularity);
//        if (report.indexOf(".zip") < 0) {
//            return new ResponseEntity<>(report, HttpStatus.OK);
//        } else {
//
//            String compressedOutput = "<pre> {\"Report\":\"DSR\", \"Description\":\"Compressed Report Due to large number of records\", \"URL To Download Report from: \":\"" + report + "\"} </pre>";
//            return new ResponseEntity<>(compressedOutput, HttpStatus.OK);
//        }        
    }

    @RequestMapping(value = "/download/{file_name}", method = RequestMethod.GET)
    public void downloadFile(HttpServletResponse response, @PathVariable("file_name") String filetoDownload) throws IOException {
        File file = new File(download_folder + "/" + filetoDownload);
        log.info("File downloaded at " + file.getAbsolutePath());
        String mimeType = "application/octet-stream";

        response.setContentType(mimeType);

        /* "Content-Disposition : attachment" will be directly download, may provide save as popup, based on your browser setting*/
        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
        response.setContentLength((int) file.length());
        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }

    @RequestMapping(value = "/sushilite/r5/reports/pr", method = RequestMethod.GET)
    public ResponseEntity<String> getReportPR(@RequestParam(value = "RepositoryIdentifier", defaultValue = "") String repositoryIdentifier,
            @RequestParam(value = "RequestorID", defaultValue = "anonymous") String requestorId,
            @RequestParam(value = "BeginDate", defaultValue = "") String beginDate,
            @RequestParam(value = "EndDate", defaultValue = "") String endDate,
            @RequestParam(value = "MetricType", defaultValue = "") String metricType,
            @RequestParam(value = "DataType", defaultValue = "") String dataType,
            @RequestParam(value = "Granularity", defaultValue = "Monthly") String granularity) throws InterruptedException, Exception {

        String report = sushiLiteService.displayReportPR(requestorId, repositoryIdentifier, beginDate, endDate, metricType, dataType, granularity);

        if (report.indexOf(".zip") < 0) {
            return new ResponseEntity<>(report, HttpStatus.OK);
        } else {

            String compreessedOutput = "<pre> {\"Report\":\"PR\", \"Description\":\"Compressed Report Due to large number of records\", \"URL To Download Report from: \":\"" + report + "\"} </pre>";

            return new ResponseEntity<>(compreessedOutput, HttpStatus.OK);
        }
    }
}
