package eu.dnetlib.usagestats.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by tsampikos on 12/4/2017.
 */
@Configuration
//public class WebMvcConfiguration implements WebMvcConfigurer {
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/sushilite").setViewName("redirect:/sushilite/");
        registry.addViewController("/sushilite/").setViewName("forward:/sushilite/index.html");

        registry.addViewController("/sushilite/AR1").setViewName("redirect:/sushilite/AR1/");
        registry.addViewController("/sushilite/AR1/").setViewName("forward:/sushilite/AR1/index.html");

        registry.addViewController("/sushilite/IR1").setViewName("redirect:/sushilite/IR1/");
        registry.addViewController("/sushilite/IR1/").setViewName("forward:/sushilite/IR1/index.html");

        registry.addViewController("/sushilite/RR1").setViewName("redirect:/sushilite/RR1/");
        registry.addViewController("/sushilite/RR1/").setViewName("forward:/sushilite/RR1/index.html");

        registry.addViewController("/sushilite/JR1").setViewName("redirect:/sushilite/JR1/");
        registry.addViewController("/sushilite/JR1/").setViewName("forward:/sushilite/JR1/index.html");

        registry.addViewController("/sushilite/BR1").setViewName("redirect:/sushilite/BR1/");
        registry.addViewController("/sushilite/BR1/").setViewName("forward:/sushilite/BR1/index.html");

        registry.addViewController("/sushilite/BR2").setViewName("redirect:/sushilite/BR2/");
        registry.addViewController("/sushilite/BR2/").setViewName("forward:/sushilite/BR2/index.html");

        registry.addViewController("/sushilite/PR").setViewName("redirect:/sushilite/PR/");
        registry.addViewController("/sushilite/PR/").setViewName("forward:/sushilite/PR/index.html");
        
        registry.addViewController("/sushilite/PR_P1").setViewName("redirect:/sushilite/PR_P1/");
        registry.addViewController("/sushilite/PR_P1/").setViewName("forward:/sushilite/PR_P1/index.html");
        
        registry.addViewController("/sushilite/IR").setViewName("redirect:/sushilite/IR/");
        registry.addViewController("/sushilite/IR/").setViewName("forward:/sushilite/IR/index.html");

        registry.addViewController("/sushilite/DSR").setViewName("redirect:/sushilite/DSR/");
        registry.addViewController("/sushilite/DSR/").setViewName("forward:/sushilite/DSR/index.html");

        super.addViewControllers(registry);
    }
}
