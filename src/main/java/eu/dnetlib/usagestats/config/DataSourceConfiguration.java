package eu.dnetlib.usagestats.config;

//import org.springframework.boot.jdbc.DataSourceBuilder;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by D.Pierrakos.
 */
@Configuration
public class DataSourceConfiguration {

    private final Logger log = Logger.getLogger(this.getClass());
    //@Value("${usagestats.driverClassName}")
    //private String driverClassName;
    @Value("${usagestats.url}")
    private String dbURL;

    //@ConfigurationProperties(prefix = "usagestats")
    @Bean
    @Primary
    public DataSource getDataSource() {
//        PoolProperties poolProperties = new PoolProperties();
//        poolProperties.setUrl(dbURL);
//        poolProperties.setDriverClassName(driverClassName);
//        log.info("dbURL " + dbURL);
//        log.info("driverClassName " + driverClassName);
//
//        poolProperties.setTestOnBorrow(true);
//        poolProperties.setValidationQuery("SELECT 1");
//        poolProperties.setValidationInterval(0);
//        DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource(poolProperties);
//        return ds;
        HikariConfig hikariConfig = new HikariConfig();
        //hikariConfig.setDriverClassName("com.cloudera.impala.jdbc41.Driver");
        hikariConfig.setJdbcUrl(dbURL);
        log.info("dbURL " + dbURL);
        //log.info("driverClassName " + driverClassName);
//
//    hikariConfig.setMaximumPoolSize(5);
//        hikariConfig.setMaximumPoolSize(100);
//        hikariConfig.setIdleTimeout(300);
//        hikariConfig.setMaximumPoolSize(20);
//        hikariConfig.setConnectionTimeout(300000);
//        hikariConfig.setConnectionTimeout(120000);
//        hikariConfig.setLeakDetectionThreshold(300000);
        hikariConfig.setConnectionTestQuery("SELECT 1");
        hikariConfig.setPoolName("UsageStats_HikariCP");

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        return dataSource;
    }
}
