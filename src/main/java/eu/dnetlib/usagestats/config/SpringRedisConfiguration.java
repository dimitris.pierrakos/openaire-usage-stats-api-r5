package eu.dnetlib.usagestats.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
//@ConfigurationProperties(prefix = "usagestats")
//@EnableConfigurationProperties
public class SpringRedisConfiguration {

    private final Logger log = Logger.getLogger(this.getClass());

    @Value("${usagestats.redis.hostname}")
    private String hostname;

    @Value("${usagestats.redis.port}")
    private int port;

    public SpringRedisConfiguration() {
    }

    @Bean
    public JedisConnectionFactory connectionFactory() {
        JedisConnectionFactory connectionFactory = new JedisConnectionFactory();
        connectionFactory.setHostName(hostname);
        connectionFactory.setPort(port);
        connectionFactory.setUsePool(false);
        log.info("Opening redis connection to : " + connectionFactory.getHostName() + ":" + connectionFactory.getPort());
        return connectionFactory;
    }

    @Bean
    @Autowired
    public RedisTemplate<String, String> redisTemplate() {
        RedisTemplate<String, String> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory());
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new StringRedisSerializer());
        return redisTemplate;
    }
}
