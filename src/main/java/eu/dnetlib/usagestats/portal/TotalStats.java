package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TotalStats {
    private int repositories;
    private int items;
    private int downloads;
    private int views;
    private List<YearlyStats> yearlyStats;

    @JsonProperty("yearly_stats")
    public List<YearlyStats> getYearlyStats() {
        return yearlyStats;
    }

    public void setYearlyStats(List<YearlyStats> yearlyStats) {
        this.yearlyStats = yearlyStats;
    }


    public TotalStats() {}

    public int getRepositories() {
        return repositories;
    }

    public void setRepositories(int repositories) {
        this.repositories = repositories;
    }

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }
}
