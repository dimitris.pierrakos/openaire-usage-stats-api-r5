package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class MonthlyUsageStats implements Serializable {

    private final static long serialVersionUID = 1;


    private String date = "0";
    private String downloads = "0";
    private String views = "0";
    //private String pageviews = "0";

    public MonthlyUsageStats() {
    }
    
    @JsonProperty("date")
    public String getDate() {
        return date;
    }

    @JsonProperty("downloads")
    public String getDownloads() {
        return downloads;
    }

    @JsonProperty("views")
    public String getViews() {
        return views;
    }

    public void addDate(String date) {
        this.date=date;
    }

    public void addViews(String views) {
        this.views=views;
    }
    public void addDownloads(String downloads) {
        this.downloads=downloads;
    }

}
