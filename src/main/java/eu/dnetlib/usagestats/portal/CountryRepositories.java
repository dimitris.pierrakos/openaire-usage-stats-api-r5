package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CountryRepositories implements Serializable {

    private final static long serialVersionUID = 1;


    private String country = "";
    private String repository = "";
    //private String pageviews = "0";

    public CountryRepositories() {
    }
    
    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("repository")
    public String getRepository() {
        return repository;
    }

    public void addCountry(String country) {
        this.country=country;
    }

    public void addRepository(String repository) {
        this.repository=repository;
    }

}
