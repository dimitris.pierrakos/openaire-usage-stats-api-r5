package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TotalStatsReposViewsDownloads {
    private String repositories;
    private String downloads;
    private String views;

    @JsonProperty("repositories")
    public String getRepositories() {
        return repositories;
    }

    public void addRepositories(String repositories) {
        this.repositories=repositories;
    }

    @JsonProperty("total_views")
    public String getViews() {
        return views;
    }

    public void addViews(String views) {
        this.views=views;
    }
    @JsonProperty("total_downloads")
    public String getDownloads() {
        return downloads;
    }

    public void addDownloads(String downloads) {
        this.downloads=downloads;
    }

}
