package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by tsampikos on 8/11/2016.
 */
public class RepositoryStats implements Serializable {
    private final static long serialVersionUID = 1;
    private String datasource_name = "";
    private String datasource_id = "";
    private String value = "";
    private String openaire = "";

    public RepositoryStats() {
    }

    public RepositoryStats(String datasource_name, String datasource_id, String value, String openaire) {
        this.datasource_name = datasource_name;
        this.datasource_id = datasource_id;
        this.value = value;
        this.openaire = openaire;
    }

    @JsonProperty("datasource_name")
    public String getDatasource_name() {
        return datasource_name;
    }

    @JsonProperty("datasource_id")
    public String getDatasource_id() {
        return datasource_id;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("openaire")
    public String getOpenaire() {
        return openaire;
    }

    /*
    public String toString(){
        return datasource_name + " " + datasource_id + " " + value + " " + openaire;
    }
    */

}
