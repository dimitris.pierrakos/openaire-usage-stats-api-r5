package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class YearlyStats {
    private int year;
    private int repositories;
    private int items;
    private int downloads;
    private int views;
    private List<MonthlyStats> monthlyStats;

    @JsonProperty("monthly_stats")
    public List<MonthlyStats> getMonthlyStats() {
        return monthlyStats;
    }

    public void setMonthlyStats(List<MonthlyStats> monthlyStats) {
        this.monthlyStats = monthlyStats;
    }

    public YearlyStats() {}

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getRepositories() {
        return repositories;
    }

    public void setRepositories(int repositories) {
        this.repositories = repositories;
    }

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }
}
