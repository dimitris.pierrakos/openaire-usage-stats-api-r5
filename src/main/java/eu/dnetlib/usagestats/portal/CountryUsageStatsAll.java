package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CountryUsageStatsAll implements Serializable {

    private final static long serialVersionUID = 1;

    private String country_all = "all";
    private String downloads_all = "0";
    private String views_all = "0";
    private List<CountryUsageStats> countryUsageStats = new ArrayList<CountryUsageStats>();
    //private String pageviews = "0";

    public CountryUsageStatsAll() {
    }
    
    @JsonProperty("country")
    public String getCountry() {
        return country_all;
    }

    @JsonProperty("downloads_all")
    public String getDownloads() {
        return downloads_all;
    }

    @JsonProperty("views_all")
    public String getViews() {
        return views_all;
    }
    @JsonProperty("CountriesList")
    public List<CountryUsageStats> getCountryUsageStats() {
        return countryUsageStats;
    }

    public void addViewsAll(String views_all) {
        this.views_all=views_all;
    }
    public void addDownloadsAll(String downloads_all) {
        this.downloads_all=downloads_all;
    }

    public void addCountryUsageStats(List<CountryUsageStats> countryUsageStats) {
        this.countryUsageStats = countryUsageStats;
    }
    

}
