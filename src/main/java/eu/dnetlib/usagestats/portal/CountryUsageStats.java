package eu.dnetlib.usagestats.portal;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CountryUsageStats implements Serializable {

    private final static long serialVersionUID = 1;


    private String country = "";
    private String total_repos = "";
    private String downloads = "";
    private String views = "";
    
    //private String pageviews = "0";

    public CountryUsageStats() {
    }
    
    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("total_repos")
    public String getTotalRepos() {
        return total_repos;
    }

    @JsonProperty("downloads")
    public String getDownloads() {
        return downloads;
    }

    @JsonProperty("views")
    public String getViews() {
        return views;
    }

    public void addCountry(String country) {
        this.country=country;
    }

    public void addTotalRepos(String total_repos) {
        this.total_repos=total_repos;
    }

    public void addViews(String views) {
        this.views=views;
    }
    public void addDownloads(String downloads) {
        this.downloads=downloads;
    }

}
