package eu.dnetlib.usagestats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.retry.annotation.EnableRetry;

//@PropertySources({
//    @PropertySource("classpath:usageStatsAPI.properties"), //@PropertySource("classpath:dnet-override.properties")
//}
//)
@EnableRetry
@SpringBootApplication
public class UsageStatsApi  {

    public static void main(String[] args) {
        SpringApplication.run(UsageStatsApi.class, args);
    }
}



