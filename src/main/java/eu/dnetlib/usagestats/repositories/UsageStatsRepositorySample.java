package eu.dnetlib.usagestats.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.usagestats.portal.*;
import eu.dnetlib.usagestats.sushilite.domain.*;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by D.Pierrakos
 */
@Repository
public class UsageStatsRepositorySample {

    private final DataSource usageStatsDB;

    private final HashOperations<String, String, String> jedis;

    private final Logger log = Logger.getLogger(this.getClass());
    @Value("${prod.statsdb}")
    private String statsDB;
    @Value("${prod.usagestatsImpalaDB}")
    private String usagestatsImpalaDB;

    public UsageStatsRepositorySample(DataSource usageStatsDB,
                                      RedisTemplate<String, String> redisTemplate) {
        this.usageStatsDB = usageStatsDB;
        this.jedis = redisTemplate.opsForHash();
    }

    private static String MD5(String string) throws java.security.NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(string.getBytes());

        byte byteData[] = md.digest();
        StringBuilder sb = new StringBuilder();
        for (byte aByteData : byteData) {
            sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    private static String toJson(Object o) throws com.fasterxml.jackson.core.JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(o);
    }

    private static UsageStats fromJson(String string) throws java.io.IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(string, UsageStats.class);
    }

    /*
    private static List<ReportItem> reportItemsFromJson(String string) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(string, objectMapper.getTypeFactory().constructCollectionType(List.class, COUNTER_Platform_Usage.class));
    }
     */
    public List<MonthlyUsageStats> executeMontlyUsageStats(String query) {
        List<MonthlyUsageStats> montlhyList = new ArrayList<MonthlyUsageStats>();

        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            connection = usageStatsDB.getConnection();
            log.info(connection.toString());
            st = connection.prepareStatement(query);
            log.info(st.toString());
            rs = st.executeQuery();
            while (rs.next()) {
                MonthlyUsageStats monthlyUsageStats = new MonthlyUsageStats();
                monthlyUsageStats.addDate(rs.getString(1));
                monthlyUsageStats.addDownloads(rs.getString(2));
                monthlyUsageStats.addViews(rs.getString(3));
                montlhyList.add(monthlyUsageStats);
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            jedis.put("test", "result", toJson(montlhyList));
            jedis.put("test", "persistent", "false");
            jedis.put("test", "fetchMode", "3");
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return montlhyList;
    }

    public TotalStatsReposViewsDownloads executeTotalStatsReposViewsDownloads(
            String query) {
        TotalStatsReposViewsDownloads totalStatsReposViewsDownlads = new TotalStatsReposViewsDownloads();

        String total_repos = " ";
        String views = " ";
        String downloads = " ";
        String redis_key = "";
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            connection = usageStatsDB.getConnection();
            log.info(connection.toString());
            st = connection.prepareStatement(query);
            log.info(st.toString());
            rs = st.executeQuery();
            redis_key = MD5(st.toString());
            while (rs.next()) {
                totalStatsReposViewsDownlads.addRepositories(rs.getString(1));
                totalStatsReposViewsDownlads.addViews(rs.getString(2));
                totalStatsReposViewsDownlads.addDownloads(rs.getString(3));
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            jedis.put(redis_key, "result", toJson(totalStatsReposViewsDownlads));
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "fetchMode", "3");
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return totalStatsReposViewsDownlads;
    }

    public CountryUsageStatsAll executeCountryUsageStats(String query) {
        CountryUsageStatsAll countryListAll = new CountryUsageStatsAll();

        List<CountryUsageStats> countryList = new ArrayList<CountryUsageStats>();

        String date = " ";
        String total_repos = " ";
        String views = " ";
        String downloads = " ";
        String redis_key = "redis_key";
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int total_views = 0;
        int total_downloads = 0;

        try {
            connection = usageStatsDB.getConnection();
            log.info(connection.toString());
            st = connection.prepareStatement(query);
            log.info(st.toString());
            rs = st.executeQuery();
            redis_key = MD5(st.toString());
            while (rs.next()) {
                CountryUsageStats countryUsageStats = new CountryUsageStats();
                countryUsageStats.addCountry(rs.getString(1));
                countryUsageStats.addTotalRepos(rs.getString(2));
                countryUsageStats.addViews(rs.getString(3));
                countryUsageStats.addDownloads(rs.getString(4));
                total_views += Integer.parseInt(rs.getString(3));
                total_downloads += Integer.parseInt(rs.getString(4));

                countryList.add(countryUsageStats);
            }
            countryListAll.addViewsAll(Integer.toString(total_views));
            countryListAll.addDownloadsAll(Integer.toString(total_downloads));

            countryListAll.addCountryUsageStats(countryList);

        } catch (Exception e) {
            log.info(e);
            System.out.println(e);
        }

        try {
            jedis.put(redis_key, "result", toJson(countryListAll));
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "fetchMode", "3");
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
        return countryListAll;
    }

    public CountryUsageStats executeCountryUsageStats(String query,
            String country) {
        CountryUsageStats countryUsageStats = new CountryUsageStats();
        String total_repos = " ";
        String views = " ";
        String downloads = " ";
        String redis_key = "";
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int total_views = 0;
        int total_downloads = 0;

        try {
            connection = usageStatsDB.getConnection();
            st = connection.prepareStatement(query);

            redis_key = MD5(st.toString());
            //st.setString(1, country);
            log.info(st.toString());
            rs = st.executeQuery();

            while (rs.next()) {
                countryUsageStats.addCountry(country);
                countryUsageStats.addTotalRepos(rs.getString(1));
                countryUsageStats.addViews(rs.getString(2));
                countryUsageStats.addDownloads(rs.getString(3));

            }

        } catch (Exception e) {
            log.info(e);
            System.out.println(e);
        }

        try {
            jedis.put(redis_key, "result", toJson(countryUsageStats));
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "fetchMode", "3");
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
        return countryUsageStats;
    }

    public List<CountryRepositories> executeCountryRepositories(String query) {

        List<CountryRepositories> countryReposList = new ArrayList<CountryRepositories>();

        String country = " ";
        String repository = " ";
        String redis_key = "";
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            connection = usageStatsDB.getConnection();
            log.info(connection.toString());
            st = connection.prepareStatement(query);
            log.info(st.toString());
            rs = st.executeQuery();
            redis_key = MD5(st.toString());
            while (rs.next()) {
                CountryRepositories countryRepository = new CountryRepositories();
                countryRepository.addCountry(rs.getString(1));
                countryRepository.addRepository(rs.getString(2));
                countryReposList.add(countryRepository);
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            jedis.put(redis_key, "result", toJson(countryReposList));
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "fetchMode", "3");
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return countryReposList;
    }

    public List<MonthlyUsageStats> executeMontlyUsageStatsForRepo(String query,
            String datasourceId) {
        List<MonthlyUsageStats> montlhyList = new ArrayList<MonthlyUsageStats>();

        String redis_key = "";
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            connection = usageStatsDB.getConnection();
            st = connection.prepareStatement(query);
            redis_key = MD5(st.toString());
            st.setString(1, datasourceId);
            log.info(connection.toString());
            rs = st.executeQuery();
            while (rs.next()) {
                MonthlyUsageStats monthlyUsageStats = new MonthlyUsageStats();
                monthlyUsageStats.addDate(rs.getString(1));
                monthlyUsageStats.addDownloads(rs.getString(2));
                monthlyUsageStats.addViews(rs.getString(3));
                montlhyList.add(monthlyUsageStats);
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            jedis.put(redis_key, "result", toJson(montlhyList));
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "fetchMode", "3");
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return montlhyList;
    }

    public UsageStats executeUsageStats(String query, List<String> values,
            String type) {

        UsageStats usageStats = new UsageStats();
        int total_views = 0;
        int total_downloads = 0;
        int page_views = 0;
        int openaire_downloads = 0;
        int openaire_views = 0;
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            connection = usageStatsDB.getConnection();
            st = connection.prepareStatement(query);
            int i = 1;
            for (String s : values) {
                st.setString(i, s);
                i++;
            }

            String redis_key = MD5(st.toString());

            String redis_result = jedis.get(redis_key, "result");
            if (redis_result != null) {
                return fromJson(redis_result);
            }

            rs = st.executeQuery();
            if (type.equals("result")) {
                while (rs.next()) {
                    if (rs.getString(1).equals("views") && rs.getString(4) != null && !rs.getString(4).equals("") && !rs.getString(4).equals("null")) {
                        usageStats.addViews(new RepositoryStats(rs.getString(3), rs.getString(2), rs.getString(4), rs.getString(5)));
                        total_views += Integer.parseInt(rs.getString(4));
                        openaire_views += Integer.parseInt(rs.getString(5));
                    } else if (rs.getString(1).equals("downloads") && rs.getString(4) != null && !rs.getString(4).equals("") && !rs.getString(4).equals("null")) {
                        usageStats.addDownloads(new RepositoryStats(rs.getString(3), rs.getString(2), rs.getString(4), "0"));
                        total_downloads += Integer.parseInt(rs.getString(4));
                        openaire_downloads += Integer.parseInt(rs.getString(5));
                    } else if (rs.getString(1).equals("pageviews") && rs.getString(4) != null && !rs.getString(4).equals("") && !rs.getString(4).equals("null")) {
                        page_views = Integer.parseInt(rs.getString(4));
                    }
                }
                usageStats.setTotal_views(Integer.toString(total_views));
                usageStats.setTotal_downloads(Integer.toString(total_downloads));
                usageStats.setPageViews(Integer.toString(page_views));
                usageStats.setTotal_openaire_views(Integer.toString(openaire_views));
                usageStats.setTotal_openaire_downloads(Integer.toString(openaire_downloads));
            } else if (type.equals("project") || type.equals("datasource")) {
                while (rs.next()) {
                    if (rs.getString(1).equals("views") && rs.getString(2) != null && !rs.getString(2).equals("") && !rs.getString(2).equals("null")) {
                        total_views += Integer.parseInt(rs.getString(2));
                        openaire_views += Integer.parseInt(rs.getString(3));
                    } else if (rs.getString(1).equals("downloads") && rs.getString(2) != null && !rs.getString(2).equals("") && !rs.getString(2).equals("null")) {
                        total_downloads += Integer.parseInt(rs.getString(2));
                        openaire_downloads += Integer.parseInt(rs.getString(3));
                    } else if (rs.getString(1).equals("pageviews") && rs.getString(2) != null && !rs.getString(2).equals("") && !rs.getString(2).equals("null")) {
                        page_views = Integer.parseInt(rs.getString(2));
                    }
                    /*
                    else if (rs.getString(1).equals("openaire") && rs.getString(2) != null && !rs.getString(2).equals("") && !rs.getString(2).equals("null")) {
                        openaire = Integer.parseInt(rs.getString(2));
                    }
                     */

                }
                usageStats.setTotal_views(Integer.toString(total_views));
                usageStats.setTotal_downloads(Integer.toString(total_downloads));
                usageStats.setPageViews(Integer.toString(page_views));
                usageStats.setTotal_openaire_views(Integer.toString(openaire_views));
                usageStats.setTotal_openaire_downloads(Integer.toString(openaire_downloads));
            }

            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "query", st.toString());
            //jedis.put(redis_key, "result", toString(usageStats));
            jedis.put(redis_key, "result", toJson(usageStats));
            jedis.put(redis_key, "fetchMode", "3");

        } catch (Exception e) {
            log.error("Cannot execute query2 : ", e);

        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
        return usageStats;
    }

    public TotalStats executeTotalStats() {
        TotalStats totalStats = null;
        try {
            String redis_result = jedis.get("total_stats", "result");
            if (redis_result != null) {
                totalStats = fromJsonTotalStats(redis_result);
            } else {
                return updateTotalStats();
            }
        } catch (Exception e) {
            log.error("Cannot execute totalStats : ", e);
        }
        return totalStats;
    }

    public TotalStats updateTotalStats() {
        TotalStats totalStats = new TotalStats();
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        HashMap<Integer, List<MonthlyStats>> monthlyStatsMap = new HashMap<>();

        try {
            connection = usageStatsDB.getConnection();
            //st = connection.prepareStatement("SELECT count(distinct d.repository_id) AS repository, count(distinct d.result_id) AS items, sum(d.count) AS downloads, sum(v.count) AS views from public.downloads_stats d FULL OUTER JOIN public.views_stats v ON d.source=v.source AND d.repository_id=v.repository_id AND d.result_id=v.result_id AND d.date=v.date;");
            st = connection.prepareStatement("SELECT ndv(distinct repository_id) AS repository, ndv(distinct result_id) AS items, sum(downloads) AS downloads, sum(views) AS views FROM " + usagestatsImpalaDB + ".usage_stats;");
            rs = st.executeQuery();
            rs.next();
            totalStats.setRepositories(rs.getInt(1));
            totalStats.setItems(rs.getInt(2));
            totalStats.setDownloads(rs.getInt(3));
            totalStats.setViews(rs.getInt(4));
            rs.close();
            st.close();

            //st = connection.prepareStatement("select coalesce(d.date,v.date) as month, count(distinct d.repository_id) as repository, count(distinct d.result_id) as items, sum(d.count) as downloads, sum(v.count) as views from public.downloads_stats d FULL OUTER JOIN public.views_stats v ON d.source=v.source AND d.repository_id=v.repository_id AND d.result_id=v.result_id AND d.date=v.date group by month order by month;");
            st = connection.prepareStatement("SELECT `date`, ndv(distinct repository_id) AS repository, ndv(distinct result_id) AS items, sum(downloads) AS downloads, sum(views) AS views FROM " + usagestatsImpalaDB + ".usage_stats GROUP BY `date` ORDER BY `date`;");
            rs = st.executeQuery();
            while (rs.next()) {
                int year = Integer.parseInt(rs.getString(1).substring(0, 4));
                int month = Integer.parseInt(rs.getString(1).substring(5));
                MonthlyStats monthlyStats = new MonthlyStats();
                monthlyStats.setMonth(month);
                monthlyStats.setRepositories(rs.getInt(2));
                monthlyStats.setItems(rs.getInt(3));
                monthlyStats.setDownloads(rs.getInt(4));
                monthlyStats.setViews(rs.getInt(5));

                if (monthlyStatsMap.get(year) != null) {
                    monthlyStatsMap.get(year).add(monthlyStats);
                } else {
                    List<MonthlyStats> newList = new ArrayList<>();
                    newList.add(monthlyStats);
                    monthlyStatsMap.put(year, newList);

                }
            }
            rs.close();
            st.close();

            //st = connection.prepareStatement("SELECT COALESCE(SUBSTRING(d.date FROM 1 FOR 4), SUBSTRING(v.date FROM 1 FOR 4)) AS year, COUNT(DISTINCT d.repository_id) AS repository, COUNT(DISTINCT d.result_id) AS items, SUM(d.count) AS downloads, SUM(v.count) AS views FROM public.downloads_stats d FULL OUTER JOIN public.views_stats v ON d.source=v.source AND d.repository_id=v.repository_id AND d.result_id=v.result_id AND d.date=v.date GROUP BY year ORDER BY year;");
            st = connection.prepareStatement("SELECT SUBSTR(`date`,1,4) AS year, ndv(DISTINCT repository_id) AS repository, \n"
                    + "ndv(DISTINCT result_id) AS items, SUM(downloads) AS downloads, SUM(views) AS views \n"
                    + "FROM " + usagestatsImpalaDB + ".usage_stats GROUP BY year ORDER BY year;");
            rs = st.executeQuery();
            List<YearlyStats> yearlyStatsList = new ArrayList<>();
            while (rs.next()) {
                YearlyStats yearlyStats = new YearlyStats();
                yearlyStats.setYear(rs.getInt(1));
                yearlyStats.setRepositories(rs.getInt(2));
                yearlyStats.setItems(rs.getInt(3));
                yearlyStats.setDownloads(rs.getInt(4));
                yearlyStats.setViews(rs.getInt(5));
                yearlyStats.setMonthlyStats(monthlyStatsMap.get(rs.getInt(1)));
                yearlyStatsList.add(yearlyStats);
            }
            totalStats.setYearlyStats(yearlyStatsList);
            jedis.put("total_stats", "result", toJson(totalStats));
            jedis.put("total_stats", "persistent", "false");

        } catch (Exception e) {
            log.error("Cannot execute totalStats : ", e);

        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
        return totalStats;
    }

    private static TotalStats fromJsonTotalStats(String string) throws java.io.IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(string, TotalStats.class);
    }

    public String executeRepoId(String repositoryIdentifier, String report) {
        PreparedStatement st = null;
        Connection connection = null;
        ResultSet rs = null;
        log.info("database " + statsDB);
        try {
            connection = usageStatsDB.getConnection();
            String[] split = repositoryIdentifier.split(":");
            String openaire_id = "-1";
            switch (split[0].toLowerCase()) {
                case "openaire":
                    if (!report.equals("jr1")) {
                        st = connection.prepareStatement("select id from " + statsDB + ".datasource where id=?");
                        st.setString(1, repositoryIdentifier.replaceFirst(split[0] + ":", ""));
                    } else {
                        st = connection.prepareStatement("select id from " + statsDB + ".datasource where id=? AND (type='Journal' OR type='Journal Aggregator/Publisher')");
                        st.setString(1, repositoryIdentifier.replaceFirst(split[0] + ":", ""));
                    }

                    rs = st.executeQuery();
                    while (rs.next()) {
                        openaire_id = rs.getString(1);
                    }
                    return openaire_id;

                case "opendoar":
                    if (!report.equals("jr1")) {
                        st = connection.prepareStatement("select id from " + statsDB + ".datasource_oids where oid=?");
                        st.setString(1, "opendoar____::" + repositoryIdentifier.replaceFirst(split[0] + ":", ""));
                    } else {
                        st = connection.prepareStatement("select distinct d.id from " + statsDB + ".datasource d, " + statsDB + ".datasource_oids di where di.oid=? and d.id=di.id and (type='Journal' OR type='Journal Aggregator/Publisher')");
                        st.setString(1, "opendoar____::" + repositoryIdentifier.replaceFirst(split[0] + ":", ""));
                    }

                    rs = st.executeQuery();
                    while (rs.next()) {
                        openaire_id = rs.getString(1);
                    }
                    return openaire_id;
                case "issn":
                    st = connection.prepareStatement("select distinct d.id from " + statsDB + ".datasource d, " + statsDB + ".datasource_oids di, " + statsDB + ".datasource_results dr where d.id=dr.id and di.oid like ? and d.id=di.id and (type='Journal' OR type='Journal Aggregator/Publisher')");
                    st.setString(1, "%" + repositoryIdentifier.replaceFirst(split[0] + ":", "") + "%");

                    rs = st.executeQuery();
                    while (rs.next()) {
                        openaire_id = rs.getString(1);
                    }
                    return openaire_id;
                default:
                    return "-1";
            }
        } catch (Exception e) {
            log.error("Repository id failed: ", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
        return "-1";
    }

    public String getInstitutionID(String repositoryIdentifier) {
        PreparedStatement st = null;
        Connection connection = null;
        ResultSet rs = null;
        log.info("database " + statsDB);
        try {
            connection = usageStatsDB.getConnection();
            String[] split = repositoryIdentifier.split(":");
            String openaire_id = "-1";
            switch (split[0].toLowerCase()) {
                case "openaire":
                    st = connection.prepareStatement("select id from " + statsDB + ".datasource where id=?");
                    st.setString(1, repositoryIdentifier.replaceFirst(split[0] + ":", ""));

                    rs = st.executeQuery();
                    while (rs.next()) {
                        openaire_id = rs.getString(1);
                    }
                    return openaire_id;

                case "opendoar":
                    st = connection.prepareStatement("select id from " + statsDB + ".datasource_oids where oid=?");
                    st.setString(1, "opendoar____::" + repositoryIdentifier.replaceFirst(split[0] + ":", ""));
                    rs = st.executeQuery();
                    while (rs.next()) {
                        openaire_id = rs.getString(1);
                    }
                    return openaire_id;
                case "issn":
                    st = connection.prepareStatement("select distinct d.id from " + statsDB + ".datasource d, " + statsDB + ".datasource_oids di, " + statsDB + ".datasource_results dr where d.id=dr.id and di.oid like ? and d.id=di.id and (type='Journal' OR type='Journal Aggregator/Publisher')");
                    st.setString(1, "%" + repositoryIdentifier.replaceFirst(split[0] + ":", "") + "%");

                    rs = st.executeQuery();
                    while (rs.next()) {
                        openaire_id = rs.getString(1);
                    }
                    return openaire_id;
                default:
                    return "-1";
            }
        } catch (Exception e) {
            log.error("Repository id failed: ", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
        return "-1";
    }

    public void executeItemIR(List<COUNTER_Item_Usage> reportItems, String repositoryIdentifier, String itemIdentifier, Date beginDate,
            Date endDate, List<String> metricType, String itemDataType, String granularity) {

        String[] split = itemIdentifier.split(":");
        switch (split[0].toLowerCase()) {
            case "oid":
                executeOid(reportItems, repositoryIdentifier, itemIdentifier.replaceFirst(split[0] + ":", ""), beginDate, endDate, metricType, itemDataType, granularity);
                break;
            case "doi":
                executeDoi(reportItems, repositoryIdentifier, itemIdentifier.replaceFirst(split[0] + ":", ""), beginDate, endDate, metricType, itemDataType, granularity);
                break;
//            case "openaire":
//                executeOpenaire(reportItems, itemIdentifier.replaceFirst(split[0] + ":", ""), repositoryIdentifier, itemDataType, beginDate, endDate, granularity);
//                break;
            default:
        }
    }

    public void executeItemDSR(List<COUNTER_Dataset_Usage> reportItems, String repositoryIdentifier, String itemIdentifier, Date beginDate,
            Date endDate, List<String> metricType, String granularity) {

        String[] split = itemIdentifier.split(":");
        switch (split[0].toLowerCase()) {
            case "oid":
                executeDSROid(reportItems, repositoryIdentifier, itemIdentifier.replaceFirst(split[0] + ":", ""), beginDate, endDate, metricType, granularity);
                break;
            case "doi":
                executeDSRDoi(reportItems, repositoryIdentifier, itemIdentifier.replaceFirst(split[0] + ":", ""), beginDate, endDate, metricType, granularity);
                break;
//            case "openaire":
//                executeOpenaire(reportItems, itemIdentifier.replaceFirst(split[0] + ":", ""), repositoryIdentifier, itemDataType, beginDate, endDate, granularity);
//                break;
            default:
        }
    }

    private void executeOid(List<COUNTER_Item_Usage> reportItems, String repositoryIdentifier,
            String oid, Date beginDate, Date endDate, List<String> metricType, String itemDataType, String granularity) {
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            connection = usageStatsDB.getConnection();
            //st = connection.prepareStatement("SELECT DISTINCT roid.id FROM public.result_oids roid, public.downloads_stats s WHERE s.result_id=roid.id AND roid.orid=? UNION SELECT DISTINCT roid.id FROM public.result_oids roid, public.views_stats s WHERE s.result_id=roid.id AND roid.orid=?");
            st = connection.prepareStatement("SELECT DISTINCT roid.id FROM " + statsDB + ".result_oids roid, " + usagestatsImpalaDB + ".usage_stats us WHERE us.result_id=roid.id AND roid.oid=?");
            st.setString(1, oid);
            //st.setString(2, oid);

            rs = st.executeQuery();

            while (rs.next()) {
                if (repositoryIdentifier != null) {
                    executeBatchItemsIR(reportItems, repositoryIdentifier, rs.getString(1), beginDate, endDate, metricType, itemDataType, granularity);
                } else {
                    executeItemsAllRepoIR(reportItems, rs.getString(1), beginDate, endDate, metricType, itemDataType, granularity);
                }
            }
            connection.close();
        } catch (Exception e) {
            log.error("Oid to OpenAIRE id failed: ", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
    }

    private void executeDoi(List<COUNTER_Item_Usage> reportItems, String repositoryIdentifier,
            String doi, Date beginDate, Date endDate, List<String> metricType, String itemDataType, String granularity) {
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = usageStatsDB.getConnection();
            //st = connection.prepareStatement("SELECT DISTINCT poid.id FROM public.result_pids poid, public.downloads_stats s WHERE s.result_id=poid.id AND poid.type='doi' AND poid.pid=? UNION SELECT DISTINCT poid.id FROM public.result_pids poid, public.views_stats s WHERE s.result_id=poid.id AND poid.type='doi' AND poid.pid=?");
            st = connection.prepareStatement("SELECT DISTINCT poid.id FROM " + statsDB + ".result_pids poid, " + usagestatsImpalaDB + ".usage_stats us WHERE us.result_id=poid.id AND poid.type='Digital Object Identifier' AND poid.pid=?");
            st.setString(1, doi);
            //st.setString(2, doi);

            rs = st.executeQuery();

            while (rs.next()) {
                if (repositoryIdentifier != null) {
                    executeBatchItemsIR(reportItems, repositoryIdentifier, rs.getString(1), beginDate, endDate, metricType, itemDataType, granularity);
                } else {
                    executeItemsAllRepoIR(reportItems, rs.getString(1), beginDate, endDate, metricType, itemDataType, granularity);
                }
            }
        } catch (Exception e) {
            log.error("Doi to OpenAIRE id failed: ", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
    }

    private void executeDSROid(List<COUNTER_Dataset_Usage> reportItems, String repositoryIdentifier,
            String oid, Date beginDate, Date endDate, List<String> metricType, String granularity) {
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            connection = usageStatsDB.getConnection();
            //st = connection.prepareStatement("SELECT DISTINCT roid.id FROM public.result_oids roid, public.downloads_stats s WHERE s.result_id=roid.id AND roid.orid=? UNION SELECT DISTINCT roid.id FROM public.result_oids roid, public.views_stats s WHERE s.result_id=roid.id AND roid.orid=?");
            st = connection.prepareStatement("SELECT DISTINCT roid.id FROM " + statsDB + ".result_oids roid, " + usagestatsImpalaDB + ".usage_stats us, "
                    + statsDB + ".result_classifications rc "
                    + "WHERE us.result_id=roid.id AND rc.id=us.result_id AND rc.type='Dataset' AND roid.oid=?");
            st.setString(1, oid);
            //st.setString(2, oid);

            rs = st.executeQuery();

            while (rs.next()) {
                if (repositoryIdentifier != null) {
                    if (checkIfDatacite(repositoryIdentifier)) {
                        executeBatchItemsDSRDatacite(reportItems, repositoryIdentifier, rs.getString(1), beginDate, endDate, metricType, granularity);
                    } else {
                        executeBatchItemsDSR(reportItems, repositoryIdentifier, rs.getString(1), beginDate, endDate, metricType, granularity);
                    }
                } else {
//                    if (checkIfDatacite(repositoryIdentifier)) {
//                        executeItemsAllRepoDSRDatacite(reportItems, rs.getString(1), beginDate, endDate, metricType, granularity);
//                    } else {
                        executeItemsAllRepoDSR(reportItems, rs.getString(1), beginDate, endDate, metricType, granularity);
//                    }
                }
            }
        } catch (Exception e) {
            log.error("Oid to OpenAIRE id failed: ", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
    }

    private void executeDSRDoi(List<COUNTER_Dataset_Usage> reportItems, String repositoryIdentifier,
            String doi, Date beginDate, Date endDate, List<String> metricType, String granularity) {
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = usageStatsDB.getConnection();
            //st = connection.prepareStatement("SELECT DISTINCT poid.id FROM public.result_pids poid, public.downloads_stats s WHERE s.result_id=poid.id AND poid.type='doi' AND poid.pid=? UNION SELECT DISTINCT poid.id FROM public.result_pids poid, public.views_stats s WHERE s.result_id=poid.id AND poid.type='doi' AND poid.pid=?");
            st = connection.prepareStatement("SELECT DISTINCT poid.id FROM " + statsDB + ".result_pids poid, " + usagestatsImpalaDB + ".usage_stats us, "
                    + statsDB + ".result_classifications rc "
                    + "WHERE us.result_id=poid.id AND poid.type='Digital Object Identifier' AND rc.id=us.result_id AND rc.type='Dataset' AND poid.pid=?");
            st.setString(1, doi);
            //st.setString(2, doi);

            rs = st.executeQuery();

            while (rs.next()) {
                if (repositoryIdentifier != null) {
                    if (checkIfDatacite(repositoryIdentifier)) {
                        executeBatchItemsDSRDatacite(reportItems, repositoryIdentifier, rs.getString(1), beginDate, endDate, metricType, granularity);
                    } else {
                        executeBatchItemsDSR(reportItems, repositoryIdentifier, rs.getString(1), beginDate, endDate, metricType, granularity);
                    }
                } else {
//                    if (checkIfDatacite(repositoryIdentifier)) {
//                        executeItemsAllRepoDSRDatacite(reportItems, rs.getString(1), beginDate, endDate, metricType, granularity);
//                    } else {
                        executeItemsAllRepoDSR(reportItems, rs.getString(1), beginDate, endDate, metricType, granularity);
//                    }
                }
            }
        } catch (Exception e) {
            log.error("Doi to OpenAIRE id failed: ", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
    }

    public void executeBatchItemsPR(List<COUNTER_Platform_Usage> reportItems,
            String repositoryIdentifier, Date beginDate,
            Date endDate, String metricType, String dataType, String granularity) {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
        String beginDateStr = postgresFormat.format(beginDate);
        String endDateStr = postgresFormat.format(endDate);

        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String platform = getInstitutionName(repositoryIdentifier);
        log.info("Platform " + platform);
        log.info("Data Type " + dataType);

        try {
            connection = usageStatsDB.getConnection();
            if (granularity.equalsIgnoreCase("totals")) {
                if (dataType.equals("") || dataType.equals("All")) {
                    st = connection.prepareStatement("SELECT rc.type, sum(us.total_item_requests) as total_item_requests, "
                            +"sum(us.total_item_investigations) as total_item_investigations, "
                            +"sum(us.unique_item_requests) as unique_item_requests, "
                            +"sum(us.unique_item_investigations) as unique_item_investigations "
                            + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us, "
                            + statsDB + ".result_classifications rc WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND rc.id=us.result_id GROUP BY rc.type order by rc.type ASC limit 10;");

                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);

                    rs = st.executeQuery();
                    COUNTER_Platform_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};
                    if (metricType.contains("Total_Item_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Item_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Item_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Item_Investigations")) {
                        metricTypeValue[3] = true;
                    }
                    while (rs.next()) {
                        reportItem = new COUNTER_Platform_Usage(platform, rs.getString(1), "Regular");
                        reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(4) : null, metricTypeValue[3] == true ? rs.getString(5) : null));
                        reportItems.add(reportItem);
                    }

                } else {
                    st = connection.prepareStatement("SELECT rc.type, sum(us.total_item_requests) as total_item_requests, "
                            +"sum(us.total_item_investigations) as total_item_investigations, "
                            +"sum(us.unique_item_requests) as unique_item_requests, "
                            +"sum(us.unique_item_investigations) as unique_item_investigations "
                            +"FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us, "
                            + statsDB + ".result_classifications rc WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND rc.type=? AND rc.id=us.result_id GROUP BY rc.type order by rc.type ASC limit 10;");

                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);
                    st.setString(4, dataType);

                    rs = st.executeQuery();
                    COUNTER_Platform_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};

                    if (metricType.contains("Total_Item_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Item_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Item_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Item_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    while (rs.next()) {
                        reportItem = new COUNTER_Platform_Usage(platform, rs.getString(1), "Regular");
                        reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(4) : null, metricTypeValue[3] == true ? rs.getString(5) : null));
                        reportItems.add(reportItem);
                    }

                }
            } else if (granularity.equalsIgnoreCase("monthly")) {
                if (dataType.equals("") || dataType.equals("All")) {
                    st = connection.prepareStatement("SELECT rc.type, us.`date`, sum(us.total_item_requests) as total_item_requests, "
                            +"sum(us.total_item_investigations) as total_item_investigations, "
                            +"sum(us.unique_item_requests) as unique_item_requests, "
                            +"sum(us.unique_item_investigations) as unique_item_investigations "
                            + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us, "
                            + statsDB + ".result_classifications rc WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND rc.id=us.result_id GROUP BY rc.type, us.`date` order by rc.type, us.`date` ASC limit 10;");

                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);

                    rs = st.executeQuery();
                    String result = "";
                    String lastDate = "";
                    COUNTER_Platform_Usage reportItem = null;

                    boolean[] metricTypeValue = {false, false, false, false};
                    if (metricType.contains("Total_Item_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Item_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Item_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Item_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    int ft_total = 0;
                    int abstr = 0;

                    Calendar endCal = Calendar.getInstance();
                    endCal.setTime(postgresFormat.parse(endDateStr));
                    endCal.add(Calendar.MONTH, 1);
                    Date endDateForZeros = endCal.getTime();

                    lastDate = beginDateStr;
                    String datatype = "";
                    while (rs.next()) {
                        Calendar endC = Calendar.getInstance();
                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
                        //iterate on data types
                        if (!datatype.equals(rs.getString(1))) {
                            if (reportItem != null) {
                                reportItems.add(reportItem);
                            }
                            reportItem = new COUNTER_Platform_Usage(platform, rs.getString(1), "Regular");
                            datatype = rs.getString(1);
                        }
                        if (reportItem != null) {
                            reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                    metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(5) : null, metricTypeValue[3] == true ? rs.getString(6) : null));

                        }
                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.add(Calendar.MONTH, 1);
                        lastDate = postgresFormat.format(endC.getTime());
                        //if (reportItem != null) {
                        //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                    }
                    //add last report item
                    reportItems.add(reportItem);
                } else {
                    st = connection.prepareStatement("SELECT rc.type, us.`date`, sum(us.total_item_requests) as total_item_requests, "
                            +"sum(us.total_item_investigations) as total_item_investigations, "
                            +"sum(us.unique_item_requests) as unique_item_requests, "
                            +"sum(us.unique_item_investigations) as unique_item_investigations "
                            + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us, "
                            + statsDB + ".result_classifications rc WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND rc.type=? AND rc.id=us.result_id GROUP BY rc.type, us.`date` order by rc.type, us.`date` ASC limit 10;");

                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);
                    st.setString(4, dataType);

                    rs = st.executeQuery();
                    String result = "";
                    String lastDate = "";
                    COUNTER_Platform_Usage reportItem = null;

                    boolean[] metricTypeValue = {false, false, false, false};
                    if (metricType.contains("Total_Item_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Item_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Item_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Item_Investigations")) {
                        metricTypeValue[3] = true;
                    }
                    int ft_total = 0;
                    int abstr = 0;

                    Calendar endCal = Calendar.getInstance();
                    endCal.setTime(postgresFormat.parse(endDateStr));
                    endCal.add(Calendar.MONTH, 1);
                    Date endDateForZeros = endCal.getTime();

                    lastDate = beginDateStr;
                    String datatype = "";
                    while (rs.next()) {
                        Calendar endC = Calendar.getInstance();
                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
                        //iterate on data types
                        if (!datatype.equals(rs.getString(1))) {
                            if (reportItem != null) {
                                reportItems.add(reportItem);
                            }
                            reportItem = new COUNTER_Platform_Usage(platform, rs.getString(1), "Regular");
                            datatype = rs.getString(1);
                        }
                        if (reportItem != null) {
                            reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                    metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(5) : null, metricTypeValue[3] == true ? rs.getString(6) : null));
                        }
                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.add(Calendar.MONTH, 1);
                        lastDate = postgresFormat.format(endC.getTime());
                        //if (reportItem != null) {
                        //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                    }
                    //add last report item
                    reportItems.add(reportItem);
                }

            }
            /*
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "query", st.toString());
            jedis.put(redis_key, "result", toJson(reportItems));
            jedis.put(redis_key, "fetchMode", "3");
             */
        } catch (Exception e) {
            log.error("Batch Item Report failed: ", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
    }

    public void executeBatchItemsPR_P1(List<COUNTER_Platform_Usage> reportItems,
            String repositoryIdentifier, Date beginDate,
            Date endDate) {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
        String beginDateStr = postgresFormat.format(beginDate);
        String endDateStr = postgresFormat.format(endDate);
        String platform = getInstitutionName(repositoryIdentifier);
        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = usageStatsDB.getConnection();

            st = connection.prepareStatement("SELECT us.`date`, sum(us.total_item_requests) as total_item_requests, "
                    +"sum(us.total_item_investigations) as total_item_investigations, "
                    +"sum(us.unique_item_requests) as unique_item_requests, "
                    +"sum(us.unique_item_investigations) as unique_item_investigations "
                    + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us "
                    + "WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? GROUP BY us.`date` order by us.`date` ASC limit 10;");

            st.setString(1, beginDateStr);
            st.setString(2, endDateStr);
            st.setString(3, repositoryIdentifier);

            rs = st.executeQuery();
            String result = "";
            String lastDate = "";
            COUNTER_Platform_Usage reportItem = null;

            int ft_total = 0;
            int abstr = 0;

            Calendar endCal = Calendar.getInstance();
            endCal.setTime(postgresFormat.parse(endDateStr));
            endCal.add(Calendar.MONTH, 1);
            Date endDateForZeros = endCal.getTime();

            lastDate = beginDateStr;
            String datatype = "";
            reportItem = new COUNTER_Platform_Usage(platform, null, "Regular");

            while (rs.next()) {
                Calendar endC = Calendar.getInstance();
                endC.setTime(postgresFormat.parse(rs.getString(1)));
                endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
                //iterate on data types
                reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(1))), report_dateFormat.format(endC.getTime()),
                        rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
                endC.setTime(postgresFormat.parse(rs.getString(1)));
                endC.add(Calendar.MONTH, 1);
                lastDate = postgresFormat.format(endC.getTime());
                //if (reportItem != null) {
                //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
            }
            if (reportItem.getItemPerformances().size() > 0) {
                reportItems.add(reportItem);
            }

            /*
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "query", st.toString());
            jedis.put(redis_key, "result", toJson(reportItems));
            jedis.put(redis_key, "fetchMode", "3");
             */
        } catch (Exception e) {
            log.error("Batch Item Report failed: ", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
    }

    public void executeBatchItemsIR(List<COUNTER_Item_Usage> reportItems,
            String repositoryIdentifier, String itemIdentifier, Date beginDate,
            Date endDate, List<String> metricType, String dataType, String granularity) throws Exception {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
        String beginDateStr = postgresFormat.format(beginDate);
        String endDateStr = postgresFormat.format(endDate);

        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String platform = getInstitutionName(repositoryIdentifier);

        if (itemIdentifier.equals("")) {
            try {
                connection = usageStatsDB.getConnection();

                if (granularity.equalsIgnoreCase("totals")) {
                    if (dataType.equals("")) {
                        st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                                + "SELECT type, total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations "
                                + "FROM (SELECT us.repository_id repo, rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                                + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                                + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                                + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, "
                                + "openaire_prod_stats.result rs "
                                + "WHERE us.`date`>=? AND us.`date`<=? "
                                + "AND rc.id=us.result_id AND us.result_id=rs.id AND us.repository_id=? "
                                + "GROUP BY rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp, tpd "
                                + "WHERE tpd.id=resultid GROUP BY repo,type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");

                        st.setString(1, beginDateStr);
                        st.setString(2, endDateStr);
                        st.setString(3, repositoryIdentifier);

                        rs = st.executeQuery();
                        COUNTER_Item_Usage reportItem = null;

                        boolean[] metricTypeValue = {false, false, false, false};
                        if (metricType.contains("Total_Item_Requests")) {
                            metricTypeValue[0] = true;
                        }
                        if (metricType.contains("Total_Item_Investigations")) {
                            metricTypeValue[1] = true;
                        }
                        if (metricType.contains("Unique_Item_Requests")) {
                            metricTypeValue[2] = true;
                        }
                        if (metricType.contains("Unique_Item_Investigations")) {
                            metricTypeValue[3] = true;
                        }

                        while (rs.next()) {
                            reportItem = new COUNTER_Item_Usage(rs.getString(5), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(6)), null, "Regular");
                            String[] identifiersAll = rs.getString(7).split("#!#");
                            for (int i = 0; i < identifiersAll.length; i++) {
                                String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                                reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                            }

                            reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                    metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(8) : null, metricTypeValue[3] == true ? rs.getString(9) : null));
                            reportItems.add(reportItem);
                        }

                    } else {
                        st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                                + "SELECT type, total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations "
                                + "FROM (SELECT us.repository_id repo, rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                                + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                                + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                                + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, "
                                + statsDB + ".result rs WHERE us.`date`>=? AND us.`date`<=? "
                                + "AND rc.id=us.result_id AND us.result_id=rs.id AND us.repository_id=? and rc.type=? "
                                + "GROUP BY rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp, tpd "
                                + "WHERE tpd.id=resultid GROUP BY repo,type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");

                        st.setString(1, beginDateStr);
                        st.setString(2, endDateStr);
                        st.setString(3, repositoryIdentifier);
                        st.setString(4, dataType);

                        rs = st.executeQuery();
                        COUNTER_Item_Usage reportItem = null;
                        boolean[] metricTypeValue = {false, false, false, false};

                        if (metricType.contains("Total_Item_Requests")) {
                            metricTypeValue[0] = true;
                        }
                        if (metricType.contains("Total_Item_Investigations")) {
                            metricTypeValue[1] = true;
                        }
                        if (metricType.contains("Unique_Item_Requests")) {
                            metricTypeValue[2] = true;
                        }
                        if (metricType.contains("Unique_Item_Investigations")) {
                            metricTypeValue[3] = true;
                        }

                        while (rs.next()) {
                            reportItem = new COUNTER_Item_Usage(rs.getString(5), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(6)), null, "Regular");
                            String[] identifiersAll = rs.getString(7).split("#!#");
                            for (int i = 0; i < identifiersAll.length; i++) {
                                String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                                reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                            }

                            reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                    metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(8) : null, metricTypeValue[3] == true ? rs.getString(9) : null));
                            reportItems.add(reportItem);
                        }
                    }
                } else if (granularity.equalsIgnoreCase("monthly")) {

                    if (dataType.equals("")) {
                        st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                                + "SELECT type, `date`,total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations "
                                + "FROM (SELECT us.repository_id repo, us.`date` `date`,rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                                + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                                + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                                + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, "
                                + statsDB + ".result rs WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.repository_id=? "
                                + "GROUP BY us.`date`,rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp1, tpd "
                                + "WHERE tpd.id=resultid GROUP BY repo,`date`, type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");


                        st.setString(1, beginDateStr);
                        st.setString(2, endDateStr);
                        st.setString(3, repositoryIdentifier);

                        rs = st.executeQuery();
                        String result = "";
                        String lastDate = "";
                        COUNTER_Item_Usage reportItem = null;
                        boolean[] metricTypeValue = {false, false, false, false};

                        if (metricType.contains("Total_Item_Requests")) {
                            metricTypeValue[0] = true;
                        }
                        if (metricType.contains("Total_Item_Investigations")) {
                            metricTypeValue[1] = true;
                        }
                        if (metricType.contains("Unique_Item_Requests")) {
                            metricTypeValue[2] = true;
                        }
                        if (metricType.contains("Unique_Item_Investigations")) {
                            metricTypeValue[3] = true;
                        }

                        int ft_total = 0;
                        int abstr = 0;

                        Calendar endCal = Calendar.getInstance();
                        endCal.setTime(postgresFormat.parse(endDateStr));
                        endCal.add(Calendar.MONTH, 1);
                        Date endDateForZeros = endCal.getTime();

                        lastDate = beginDateStr;
                        String datatype = "";
                        while (rs.next()) {
                            Calendar endC = Calendar.getInstance();
                            endC.setTime(postgresFormat.parse(rs.getString(2)));
                            endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                            if (!datatype.equals(rs.getString(1))) {
                                datatype = rs.getString(1);
                            }

                            reportItem = new COUNTER_Item_Usage(rs.getString(6), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(7)), null, "Regular");
                            String[] identifiersAll = rs.getString(8).split("#!#");
                            for (int i = 0; i < identifiersAll.length; i++) {
                                String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                                reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                            }
                            reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                    metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                            reportItems.add(reportItem);

                            endC.setTime(postgresFormat.parse(rs.getString(2)));
                            endC.add(Calendar.MONTH, 1);
                            lastDate = postgresFormat.format(endC.getTime());
                            //if (reportItem != null) {
                            //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                        }
                    } else {
                        st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                                + "SELECT type, `date`,total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations "
                                + "FROM (SELECT us.repository_id repo, us.`date` `date`,rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                                + "us.result_id as resultid, rs.title as item, rs.year as yop,  "
                                + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                                + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, "
                                + statsDB + ".result rs WHERE us.`date`>=? AND us.`date`<=? "
                                + "AND rc.id=us.result_id AND us.result_id=rs.id AND us.repository_id=? AND rc.type=? "
                                + "GROUP BY us.`date`,rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp, tpd "
                                + "WHERE tpd.id=resultid group by repo,`date`, type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");

                        
                        st.setString(1, beginDateStr);
                        st.setString(2, endDateStr);
                        st.setString(3, repositoryIdentifier);
                        st.setString(4, dataType);
                        rs = st.executeQuery();

                        String result = "";
                        String lastDate = "";
                        COUNTER_Item_Usage reportItem = null;
                        boolean[] metricTypeValue = {false, false, false, false};

                        if (metricType.contains("Total_Item_Requests")) {
                            metricTypeValue[0] = true;
                        }
                        if (metricType.contains("Total_Item_Investigations")) {
                            metricTypeValue[1] = true;
                        }
                        if (metricType.contains("Unique_Item_Requests")) {
                            metricTypeValue[2] = true;
                        }
                        if (metricType.contains("Unique_Item_Investigations")) {
                            metricTypeValue[3] = true;
                        }

                        int ft_total = 0;
                        int abstr = 0;

                        Calendar endCal = Calendar.getInstance();
                        endCal.setTime(postgresFormat.parse(endDateStr));
                        endCal.add(Calendar.MONTH, 1);
                        Date endDateForZeros = endCal.getTime();

                        lastDate = beginDateStr;
                        String datatype = "";
                        List<COUNTER_Item_Identifiers> identifiers = null;

                        while (rs.next()) {
                            Calendar endC = Calendar.getInstance();
                            endC.setTime(postgresFormat.parse(rs.getString(2)));
                            endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                            if (!datatype.equals(rs.getString(1))) {
//                                if (reportItem != null) {
//                                    reportItems.add(reportItem);
//                                }
//                                reportItem = new COUNTER_Platform_Usage("", "OpenAIRE", rs.getString(1), "Regular", "");
                                datatype = rs.getString(1);
                            }

                            reportItem = new COUNTER_Item_Usage(rs.getString(6), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(7)), null, "Regular");
                            String[] identifiersAll = rs.getString(8).split("#!#");
                            for (int i = 0; i < identifiersAll.length; i++) {
                                String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                                reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                            }
                            reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                    metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                            reportItems.add(reportItem);

                            //                      }
//
//                        //}
                            endC.setTime(postgresFormat.parse(rs.getString(2)));
                            endC.add(Calendar.MONTH, 1);
                            lastDate = postgresFormat.format(endC.getTime());
                            //if (reportItem != null) {
                            //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                        }
                    }
                }
                /*
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "query", st.toString());
            jedis.put(redis_key, "result", toJson(reportItems));
            jedis.put(redis_key, "fetchMode", "3");
                 */
            } catch (Exception e) {
                log.error("Batch Item Report failed: ", e);
            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(st);
                DbUtils.closeQuietly(connection);
            }
        } else {
            try {
                connection = usageStatsDB.getConnection();

                if (granularity.equalsIgnoreCase("totals")) {
                    if (dataType.equals("")) {
                        st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                                + "SELECT type, total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations "
                                + "FROM (SELECT us.repository_id repo, rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                                + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                                + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                                + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, "
                                + statsDB + ".result rs WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.repository_id=? "
                                + "AND us.result_id=? GROUP BY rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp, tpd WHERE tpd.id=resultid "
                                + "GROUP BY repo,type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");

                        st.setString(1, beginDateStr);
                        st.setString(2, endDateStr);
                        st.setString(3, repositoryIdentifier);
                        st.setString(4, itemIdentifier);

                        rs = st.executeQuery();
                        COUNTER_Item_Usage reportItem = null;
                        boolean[] metricTypeValue = {false, false, false, false};
                        if (metricType.contains("Total_Item_Requests")) {
                            metricTypeValue[0] = true;
                        }
                        if (metricType.contains("Total_Item_Investigations")) {
                            metricTypeValue[1] = true;
                        }
                        if (metricType.contains("Unique_Item_Requests")) {
                            metricTypeValue[2] = true;
                        }
                        if (metricType.contains("Unique_Item_Investigations")) {
                            metricTypeValue[3] = true;
                        }

                        while (rs.next()) {
                            reportItem = new COUNTER_Item_Usage(rs.getString(5), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(6)), null, "Regular");
                            String[] identifiersAll = rs.getString(7).split("#!#");
                            for (int i = 0; i < identifiersAll.length; i++) {
                                String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                                reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                            }

                            reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                    metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(8) : null, metricTypeValue[3] == true ? rs.getString(9) : null));
                            reportItems.add(reportItem);
                        }
                    } else {
                        st = connection.prepareStatement("WITH tpd as (select distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                                + "SELECT type, total_item_requests, total_item_investigations, resultid, item, yop, group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations "
                                + "FROM (SELECT us.repository_id repo, rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                                + "us.result_id as resultid, rs.title as item, rs.year as yop, sum(us.unique_item_requests) as unique_item_requests, "
                                + "sum(us.unique_item_investigations) as unique_item_investigations "
                                + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(select distinct id, type FROM " + statsDB + ".result_classifications) rc, "
                                + statsDB + ".result rs WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id "
                                + "AND us.repository_id=? AND us.result_id=? AND rc.type=? GROUP BY rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp, tpd "
                                + "WHERE tpd.id=resultid group by repo, type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");

                        st.setString(1, beginDateStr);
                        st.setString(2, endDateStr);
                        st.setString(3, repositoryIdentifier);
                        st.setString(4, itemIdentifier);
                        st.setString(5, dataType);

                        rs = st.executeQuery();
                        COUNTER_Item_Usage reportItem = null;
                        boolean[] metricTypeValue = {false, false, false, false};
                        if (metricType.contains("Total_Items_Requests")) {
                            metricTypeValue[0] = true;
                        }
                        if (metricType.contains("Total_Items_Ivestigations")) {
                            metricTypeValue[1] = true;
                        }

                        while (rs.next()) {
                            reportItem = new COUNTER_Item_Usage(rs.getString(5), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(6)), null, "Regular");
                            String[] identifiersAll = rs.getString(7).split("#!#");
                            for (int i = 0; i < identifiersAll.length; i++) {
                                String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                                reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                            }
                            reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                    metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(8) : null, metricTypeValue[3] == true ? rs.getString(9) : null));
                            reportItems.add(reportItem);
                        }
                    }
                } else if (granularity.equalsIgnoreCase("monthly")) {

                    if (dataType.equals("")) {
                        st = connection.prepareStatement("WITH tpd as (select distinct id, concat(type,'#-#',pid) type_id FROM  " + statsDB + ".result_pids) "
                                + "SELECT type, `date`,total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, uniquedownloads,uniqueviews "
                                + "FROM (SELECT us.repository_id repo, us.`date` `date`,rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                                + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                                + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                                + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(select distinct id, type FROM " + statsDB + ".result_classifications) rc, "
                                + statsDB + ".result rs WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.repository_id=? AND us.result_id=? "
                                + "GROUP BY us.`date`,rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp, tpd "
                                + "WHERE tpd.id=resultid group by repo,`date`, type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");

                        st.setString(1, beginDateStr);
                        st.setString(2, endDateStr);
                        st.setString(3, repositoryIdentifier);
                        st.setString(4, itemIdentifier);

                        rs = st.executeQuery();
                        String result = "";
                        String lastDate = "";
                        COUNTER_Item_Usage reportItem = null;
                        boolean[] metricTypeValue = {false, false, false, false};

                        if (metricType.contains("Total_Item_Requests")) {
                            metricTypeValue[0] = true;
                        }
                        if (metricType.contains("Total_Item_Investigations")) {
                            metricTypeValue[1] = true;
                        }
                        if (metricType.contains("Unique_Item_Requests")) {
                            metricTypeValue[2] = true;
                        }
                        if (metricType.contains("Unique_Item_Investigations")) {
                            metricTypeValue[3] = true;
                        }

                        int ft_total = 0;
                        int abstr = 0;

                        Calendar endCal = Calendar.getInstance();
                        endCal.setTime(postgresFormat.parse(endDateStr));
                        endCal.add(Calendar.MONTH, 1);
                        Date endDateForZeros = endCal.getTime();

                        lastDate = beginDateStr;
                        String datatype = "";
                        while (rs.next()) {
                            Calendar endC = Calendar.getInstance();
                            endC.setTime(postgresFormat.parse(rs.getString(2)));
                            endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                            if (!datatype.equals(rs.getString(1))) {
                                datatype = rs.getString(1);
                            }
                            reportItem = new COUNTER_Item_Usage(rs.getString(6), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(7)), null, "Regular");
                            String[] identifiersAll = rs.getString(8).split("#!#");
                            for (int i = 0; i < identifiersAll.length; i++) {
                                String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                                reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                            }
                            reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                    metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                            reportItems.add(reportItem);

                            endC.setTime(postgresFormat.parse(rs.getString(2)));
                            endC.add(Calendar.MONTH, 1);
                            lastDate = postgresFormat.format(endC.getTime());
                            //if (reportItem != null) {
                            //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                        }
                    } else {
                        st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                                + "SELECT type, `date`,total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations "
                                + "FROM (SELECT us.repository_id repo, us.`date` `date`,rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                                + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                                + "sum(us.unique_item_requests) as unique_item_requests, sum(unique_item_investigations) as unique_item_investigations "
                                + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(select distinct id, type FROM " + statsDB + ".result_classifications) rc, " + statsDB + ".result rs "
                                + "WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id "
                                + "AND us.repository_id=? AND us.result_id=? AND rc.type=? "
                                + "GROUP BY us.`date`,rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp, tpd "
                                + "WHERE tpd.id=resultid group by repo,`date`, type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");

                        st.setString(1, beginDateStr);
                        st.setString(2, endDateStr);
                        st.setString(3, repositoryIdentifier);
                        st.setString(4, itemIdentifier);
                        st.setString(5, dataType);

                        rs = st.executeQuery();
                        String result = "";
                        String lastDate = "";
                        COUNTER_Item_Usage reportItem = null;

                        boolean[] metricTypeValue = {false, false, false, false};

                        if (metricType.contains("Total_Item_Requests")) {
                            metricTypeValue[0] = true;
                        }
                        if (metricType.contains("Total_Item_Investigations")) {
                            metricTypeValue[1] = true;
                        }
                        if (metricType.contains("Unique_Item_Requests")) {
                            metricTypeValue[2] = true;
                        }
                        if (metricType.contains("Unique_Item_Investigations")) {
                            metricTypeValue[3] = true;
                        }
                        int ft_total = 0;
                        int abstr = 0;

                        Calendar endCal = Calendar.getInstance();
                        endCal.setTime(postgresFormat.parse(endDateStr));
                        endCal.add(Calendar.MONTH, 1);
                        Date endDateForZeros = endCal.getTime();

                        lastDate = beginDateStr;
                        String datatype = "";
                        List<COUNTER_Item_Identifiers> identifiers = null;

                        while (rs.next()) {
                            Calendar endC = Calendar.getInstance();
                            endC.setTime(postgresFormat.parse(rs.getString(2)));
                            endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                            if (!datatype.equals(rs.getString(1))) {
//                                if (reportItem != null) {
//                                    reportItems.add(reportItem);
//                                }
//                                reportItem = new COUNTER_Platform_Usage("", "OpenAIRE", rs.getString(1), "Regular", "");
                                datatype = rs.getString(1);
                            }

                            reportItem = new COUNTER_Item_Usage(rs.getString(6), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(7)), null, "Regular");
                            String[] identifiersAll = rs.getString(8).split("#!#");
                            for (int i = 0; i < identifiersAll.length; i++) {
                                String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                                reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                            }
                            reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                    metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                            reportItems.add(reportItem);

                            endC.setTime(postgresFormat.parse(rs.getString(2)));
                            endC.add(Calendar.MONTH, 1);
                            lastDate = postgresFormat.format(endC.getTime());
                            //if (reportItem != null) {
                            //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                        }
                    }
                }
                /*
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "query", st.toString());
            jedis.put(redis_key, "result", toJson(reportItems));
            jedis.put(redis_key, "fetchMode", "3");
                 */
            } catch (Exception e) {
                log.error("Batch Item Report failed: ", e);
            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(st);
                DbUtils.closeQuietly(connection);
            }
        }
    }

    public void executeItemsAllRepoIR(List<COUNTER_Item_Usage> reportItems,
            String itemIdentifier, Date beginDate,
            Date endDate, List<String> metricType, String dataType, String granularity) throws Exception {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
        String beginDateStr = postgresFormat.format(beginDate);
        String endDateStr = postgresFormat.format(endDate);

        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            connection = usageStatsDB.getConnection();

            if (granularity.equalsIgnoreCase("totals")) {
                if (dataType.equals("")) {
                    st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                            + "SELECT type, total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, name, unique_item_requests,unique_item_investigations "
                            + "FROM (SELECT us.repository_id repo,rc.type type, ds.name as name, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                            + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                            + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                            + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us, (SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, " + statsDB + ".result rs, "
                            + statsDB + ".datasource ds WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.result_id=? "
                            + "AND us.repository_id=ds.id GROUP BY rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id, ds.name) tmp, tpd "
                            + "WHERE tpd.id=resultid group by repo,name,  type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");


                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, itemIdentifier);

                    rs = st.executeQuery();
                    COUNTER_Item_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};
                    if (metricType.contains("Total_Item_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Item_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Item_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Item_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    while (rs.next()) {
                        reportItem = new COUNTER_Item_Usage(rs.getString(5), "OpenAIRE", rs.getString(8), rs.getString(1), Integer.toString(rs.getInt(6)), null, "Regular");
                        String[] identifiersAll = rs.getString(7).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }

                        reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                        reportItems.add(reportItem);
                    }

                } else {
                    st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                            + "SELECT type, total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, name, unique_item_requests,unique_item_investigations "
                            + "FROM (SELECT us.repository_id repo,rc.type type, ds.name as name, sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations, "
                            + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                            + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                            + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us, (SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, " + statsDB + ".result rs, "
                            + statsDB + ".datasource ds WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.result_id=? AND rc.type=? "
                            + "AND us.repository_id=ds.id GROUP BY rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id, ds.name) tmp, tpd "
                            + "WHERE tpd.id=resultid group by repo,name,  type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");

                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, itemIdentifier);
                    st.setString(4, dataType);

                    rs = st.executeQuery();
                    COUNTER_Item_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};

                    if (metricType.contains("Total_Item_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Item_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Item_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Item_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    while (rs.next()) {
                        //String platform = getInstitutionName(rs.getString(8));
                        reportItem = new COUNTER_Item_Usage(rs.getString(5), "OpenAIRE", rs.getString(8), rs.getString(1), Integer.toString(rs.getInt(6)), null, "Regular");
                        String[] identifiersAll = rs.getString(7).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }

                        reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                        reportItems.add(reportItem);
                    }

                }
            } else if (granularity.equalsIgnoreCase("monthly")) {

                if (dataType.equals("")) {

                    st = connection.prepareStatement("WITH tpd as (select distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                            + "SELECT type, `date`,total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, name, unique_item_requests,unique_item_investigations "
                            + "FROM (SELECT us.repository_id repo, us.`date` `date`,rc.type type, ds.name as name, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                            + "us.result_id as resultid, rs.title as item, rs.year as yop, sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                            + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us, (SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, " + statsDB + ".result rs, "
                            + statsDB + ".datasource ds WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.result_id=? AND us.repository_id=ds.id "
                            + "GROUP BY us.`date`,rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id, ds.name) tmp, tpd "
                            + "WHERE tpd.id=resultid group by repo,`date`,name, type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");

                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, itemIdentifier);

                    rs = st.executeQuery();
                    String result = "";
                    String lastDate = "";
                    COUNTER_Item_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};

                    if (metricType.contains("Total_Item_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Item_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Item_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Item_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    int ft_total = 0;
                    int abstr = 0;

                    Calendar endCal = Calendar.getInstance();
                    endCal.setTime(postgresFormat.parse(endDateStr));
                    endCal.add(Calendar.MONTH, 1);
                    Date endDateForZeros = endCal.getTime();

                    lastDate = beginDateStr;
                    String datatype = "";
                    while (rs.next()) {
                        Calendar endC = Calendar.getInstance();
                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                        if (!datatype.equals(rs.getString(1))) {
                            datatype = rs.getString(1);
                        }
                        reportItem = new COUNTER_Item_Usage(rs.getString(6), "OpenAIRE", rs.getString(9), rs.getString(1), Integer.toString(rs.getInt(7)), null, "Regular");
                        String[] identifiersAll = rs.getString(8).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }
                        reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(10) : null, metricTypeValue[3] == true ? rs.getString(11) : null));
                        reportItems.add(reportItem);
                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.add(Calendar.MONTH, 1);
                        lastDate = postgresFormat.format(endC.getTime());
                        //if (reportItem != null) {
                        //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                    }
                } else {
                    st = connection.prepareStatement("WITH tpd as (select distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                            + "SELECT type, `date`,total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, name, unique_item_requests,unique_item_investigations "
                            + "FROM (SELECT us.repository_id repo, us.`date` `date`,rc.type type, ds.name as name, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                            + "us.result_id as resultid, rs.title as item, rs.year as yop, sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                            + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us, (SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, " + statsDB + ".result rs, "
                            + statsDB + ".datasource ds WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.result_id=? AND us.repository_id=ds.id AND rc.type=? "
                            + "GROUP BY us.`date`,rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id, ds.name) tmp, tpd "
                            + "WHERE tpd.id=resultid group by repo,`date`,name, type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, itemIdentifier);
                    st.setString(4, dataType);

                    rs = st.executeQuery();
                    String result = "";
                    String lastDate = "";
                    COUNTER_Item_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};

                    if (metricType.contains("Total_Item_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Item_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Item_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Item_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    int ft_total = 0;
                    int abstr = 0;

                    Calendar endCal = Calendar.getInstance();
                    endCal.setTime(postgresFormat.parse(endDateStr));
                    endCal.add(Calendar.MONTH, 1);
                    Date endDateForZeros = endCal.getTime();

                    lastDate = beginDateStr;
                    String datatype = "";

                    while (rs.next()) {
                        Calendar endC = Calendar.getInstance();
                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                        if (!datatype.equals(rs.getString(1))) {
//                                if (reportItem != null) {
//                                    reportItems.add(reportItem);
//                                }
//                                reportItem = new COUNTER_Platform_Usage("", "OpenAIRE", rs.getString(1), "Regular", "");
                            datatype = rs.getString(1);
                        }
                        reportItem = new COUNTER_Item_Usage(rs.getString(6), "OpenAIRE", rs.getString(9), rs.getString(1), Integer.toString(rs.getInt(7)), null, "Regular");
                        String[] identifiersAll = rs.getString(8).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Item_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }

                        reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(10) : null, metricTypeValue[3] == true ? rs.getString(11) : null));
                        reportItems.add(reportItem);

                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.add(Calendar.MONTH, 1);
                        lastDate = postgresFormat.format(endC.getTime());
                        //if (reportItem != null) {
                        //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                    }
                }
            }
            /*
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "query", st.toString());
            jedis.put(redis_key, "result", toJson(reportItems));
            jedis.put(redis_key, "fetchMode", "3");
             */
        } catch (Exception e) {
            log.error("Batch Item Report failed: ", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
    }

    public void executeBatchItemsDSR(List<COUNTER_Dataset_Usage> reportItems,
            String repositoryIdentifier, String itemIdentifier, Date beginDate,
            Date endDate, List<String> metricType, String granularity) throws Exception {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
        String beginDateStr = postgresFormat.format(beginDate);
        String endDateStr = postgresFormat.format(endDate);

        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String platform = getInstitutionName(repositoryIdentifier);

        if (itemIdentifier.equals("")) {
            try {
                connection = usageStatsDB.getConnection();

                if (granularity.equalsIgnoreCase("totals")) {

//                    st = connection.prepareStatement("WITH tpd as (select id, concat(type,'#-#',pid) type_id from " + statsDB + ".result_pids) "
//                            + "SELECT rc.type, sum(us.downloads) as downloads, sum(us.views) as views, "
//                            + "us.result_id as resultid, rs.title as item, rs.year as yop, group_concat(distinct tpd.type_id,'#!#') as oid, "
//                            + "count(case when us.downloads >0 then 1 else null end) as uniquedownloads, count(case when us.views >0 then 1 else null end) as uniqueviews,  "
//                            + "case when us.source='Datacite' then dp.access_method else 'regular' end as access_method "
//                            + "FROM " + usagestatsImpalaDB + ".usage_stats us, " + statsDB + ".result_classifications rc, " + statsDB + ".result rs, "
//                            + statsDB + ".result_oids ro, tpd, datasetsusagestats_20210312a.datasetsperformance dp "
//                            + "WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND rc.id=us.result_id "
//                            + "AND us.result_id=rs.id AND ro.id=us.result_id AND tpd.id=us.result_id AND rc.type='Dataset' AND dp.ds_type=ro.oid "
//                            + "GROUP BY us.source, ro.id, rc.type, rs.title, us.result_id, rs.year, dp.access_method ORDER by rc.type ASC;");
                    st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                            + "SELECT type, total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations, 'Regular' as access_method "
                            + "FROM (SELECT us.repository_id repo, rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                            + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                            + "sum(us.unique_item_requests) as unique_item_requests, sum(unique_item_investigations) as unique_item_investigations "
                            + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, openaire_prod_stats.result rs "
                            + "WHERE us.`date`>='? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.repository_id=? "
                            + "AND rc.type='Dataset' GROUP BY rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp, tpd "
                            + "WHERE tpd.id=resultid GROUP BY repo,type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);

                    rs = st.executeQuery();
                    COUNTER_Dataset_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};

                    if (metricType.contains("Total_Dataset_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Dataset_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Investigations")) {
                        metricTypeValue[3] = true;
                    }
                    while (rs.next()) {
                        reportItem = new COUNTER_Dataset_Usage(rs.getString(5), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(6)), rs.getString(10));
                        String[] identifiersAll = rs.getString(7).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }

                        reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(8) : null, metricTypeValue[3] == true ? rs.getString(9) : null));
                        reportItems.add(reportItem);
                    }
                    //}
                } else if (granularity.equalsIgnoreCase("monthly")) {
//                    st = connection.prepareStatement("WITH tpd as (select id, concat(type,'#-#',pid) type_id from " + statsDB + ".result_pids) "
//                            + "SELECT rc.type, us.`date`, sum(us.downloads) as downloads, sum(us.views) as views, "
//                            + "us.result_id as resultid, rs.title as item, rs.year as yop, group_concat(distinct tpd.type_id,'#!#') as oid, "
//                            + "count(case when us.downloads >0 then 1 else null end) as uniquedownloads, count(case when us.views >0 then 1 else null end) as uniqueviews, "
//                            + "case when us.source='Datacite' then dp.access_method else 'regular' end as access_method "
//                            + "FROM " + usagestatsImpalaDB + ".usage_stats us, " + statsDB + ".result_classifications rc, " + statsDB + ".result rs, "
//                            + statsDB + ".result_oids ro, tpd, datasetsusagestats_20210312a.datasetsperformance dp  "
//                            + "WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND rc.id=us.result_id "
//                            + "AND us.result_id=rs.id AND ro.id=us.result_id AND tpd.id=us.result_id AND rc.type='Dataset' AND dp.ds_type=ro.oid "
//                            + "GROUP BY us.source, ro.id, rc.type, rs.title, us.result_id, us.`date`, rs.year, dp.access_method ORDER by rc.type, us.`date` ASC;");

                    st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                            + "SELECT type, `date`, total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations, 'Regular' as access_method "
                            + "FROM (SELECT us.repository_id repo, us.`date` as `date`, rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                            + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                            + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                            + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, openaire_prod_stats.result rs "
                            + "WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.repository_id=? "
                            + "AND rc.type='Dataset' GROUP BY us.`date`, rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp, tpd "
                            + "WHERE tpd.id=resultid GROUP BY repo,`date`, type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations ORDER BY `date` ASC limit 10;");

                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);
                    rs = st.executeQuery();

                    String result = "";
                    String lastDate = "";
                    COUNTER_Dataset_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};

                    if (metricType.contains("Total_Dataset_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Dataset_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    int ft_total = 0;
                    int abstr = 0;

                    Calendar endCal = Calendar.getInstance();
                    endCal.setTime(postgresFormat.parse(endDateStr));
                    endCal.add(Calendar.MONTH, 1);
                    Date endDateForZeros = endCal.getTime();

                    lastDate = beginDateStr;
                    String datatype = "";
                    List<COUNTER_Item_Identifiers> identifiers = null;

                    while (rs.next()) {
                        Calendar endC = Calendar.getInstance();
                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                        if (!datatype.equals(rs.getString(1))) {
//                                if (reportItem != null) {
//                                    reportItems.add(reportItem);
//                                }
//                                reportItem = new COUNTER_Platform_Usage("", "OpenAIRE", rs.getString(1), "Regular", "");
                            datatype = rs.getString(1);
                        }

                        reportItem = new COUNTER_Dataset_Usage(rs.getString(6), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(7)), rs.getString(11));
                        String[] identifiersAll = rs.getString(8).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }
                        reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                        reportItems.add(reportItem);

                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.add(Calendar.MONTH, 1);
                        lastDate = postgresFormat.format(endC.getTime());
                        //if (reportItem != null) {
                        //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                    }
                }
                /*
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "query", st.toString());
            jedis.put(redis_key, "result", toJson(reportItems));
            jedis.put(redis_key, "fetchMode", "3");
                 */
            } catch (Exception e) {
                log.error("Batch Item Report failed: ", e);
            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(st);
                DbUtils.closeQuietly(connection);
            }
        } else {
            try {
                connection = usageStatsDB.getConnection();

                if (granularity.equalsIgnoreCase("totals")) {
//                    st = connection.prepareStatement("WITH tpd as (select id, concat(type,'#-#',pid) type_id from " + statsDB + ".result_pids) "
//                            + "SELECT rc.type, sum(us.downloads) as downloads, sum(us.views) as views, "
//                            + "us.result_id as resultid, rs.title as item, rs.year as yop, group_concat(distinct tpd.type_id,'#!#') as oid, "
//                            + "count(case when us.downloads >0 then 1 else null end) as uniquedownloads, count(case when us.views >0 then 1 else null end) as uniqueviews,"
//                            + "case when us.source='Datacite' then dp.access_method else 'regular' end as access_method  "
//                            + "FROM " + usagestatsImpalaDB + ".usage_stats us, " + statsDB + ".result_classifications rc, " + statsDB + ".result rs, "
//                            + statsDB + ".result_oids ro, tpd, datasetsusagestats_20210312a.datasetsperformance dp "
//                            + "WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND us.result_id=? AND rc.id=us.result_id "
//                            + "AND us.result_id=rs.id AND ro.id=us.result_id AND tpd.id=us.result_id AND rc.type='Dataset' AND dp.ds_type=ro.oid "
//                            + "GROUP BY us.source, ro.id, rc.type, rs.title, us.result_id, rs.year, dp.access_method ORDER by rc.type ASC;");
                    st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                            + "SELECT type, total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations, 'Regular' as access_method "
                            + "FROM (SELECT us.repository_id repo, rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                            + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                            + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                            + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, openaire_prod_stats.result rs "
                            + "WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.repository_id=? AND us.result_id=? "
                            + "AND rc.type='Dataset' GROUP BY rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp, tpd "
                            + "WHERE tpd.id=resultid GROUP BY repo,type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);
                    st.setString(4, itemIdentifier);

                    rs = st.executeQuery();
                    COUNTER_Dataset_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};
                    if (metricType.contains("Total_Dataset_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Dataset_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    while (rs.next()) {
                        reportItem = new COUNTER_Dataset_Usage(rs.getString(5), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(6)), rs.getString(10));
                        String[] identifiersAll = rs.getString(7).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }
                        reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(8) : null, metricTypeValue[3] == true ? rs.getString(9) : null));
                        reportItems.add(reportItem);
                    }
                } else if (granularity.equalsIgnoreCase("monthly")) {

//                    st = connection.prepareStatement("WITH tpd as (select id, concat(type,'#-#',pid) type_id from " + statsDB + ".result_pids) "
//                            + "SELECT rc.type, us.`date`, sum(us.downloads) as downloads, sum(us.views) as views, "
//                            + "us.result_id as resultid, rs.title as item, rs.year as yop, group_concat(distinct tpd.type_id,'#!#') as oid, "
//                            + "count(case when us.downloads >0 then 1 else null end) as uniquedownloads, count(case when us.views >0 then 1 else null end) as uniqueviews,  "
//                            + "case when us.source='Datacite' then dp.access_method else 'regular' end as access_method "
//                            + "FROM " + usagestatsImpalaDB + ".usage_stats us, " + statsDB + ".result_classifications rc, " + statsDB + ".result rs, "
//                            + statsDB + ".result_oids ro, tpd, datasetsusagestats_20210312a.datasetsperformance dp  "
//                            + "WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND us.result_id=? AND rc.id=us.result_id "
//                            + "AND us.result_id=rs.id AND ro.id=us.result_id AND tpd.id=us.result_id AND rc.type='Dataset' AND dp.ds_type=ro.oid "
//                            + "GROUP BY us.source, ro.id, rc.type, rs.title, us.result_id, us.`date`, rs.year, dp.access_method ORDER by rc.type, us.`date` ASC;");
                    st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                            + "SELECT type, `date`, total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations, 'Regular' as access_method "
                            + "FROM (SELECT us.repository_id repo, us.`date` as `date`, rc.type type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                            + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                            + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                            + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, openaire_prod_stats.result rs "
                            + "WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.repository_id=? AND us.result_id=? "
                            + "AND rc.type='Dataset' GROUP BY us.`date`, rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id, us.`date`) tmp, tpd "
                            + "WHERE tpd.id=resultid GROUP BY repo,`date`, type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations ORDER BY `date` ASC limit 10;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);
                    st.setString(4, itemIdentifier);

                    rs = st.executeQuery();
                    String result = "";
                    String lastDate = "";
                    COUNTER_Dataset_Usage reportItem = null;

                    boolean[] metricTypeValue = {false, false, false, false};

                    if (metricType.contains("Total_Dataset_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Dataset_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    int ft_total = 0;
                    int abstr = 0;

                    Calendar endCal = Calendar.getInstance();
                    endCal.setTime(postgresFormat.parse(endDateStr));
                    endCal.add(Calendar.MONTH, 1);
                    Date endDateForZeros = endCal.getTime();

                    lastDate = beginDateStr;
                    String datatype = "";

                    while (rs.next()) {
                        Calendar endC = Calendar.getInstance();
                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                        if (!datatype.equals(rs.getString(1))) {
                            datatype = rs.getString(1);
                        }

                        reportItem = new COUNTER_Dataset_Usage(rs.getString(6), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(7)), rs.getString(11));
                        String[] identifiersAll = rs.getString(8).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }
                        reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                        reportItems.add(reportItem);

                        endC.setTime(postgresFormat.parse(rs.getString(2)));
                        endC.add(Calendar.MONTH, 1);
                        lastDate = postgresFormat.format(endC.getTime());
                        //if (reportItem != null) {
                        //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                    }
                }
                /*
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "query", st.toString());
            jedis.put(redis_key, "result", toJson(reportItems));
            jedis.put(redis_key, "fetchMode", "3");
                 */
            } catch (Exception e) {
                log.error("Batch Item Report failed: ", e);
            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(st);
                DbUtils.closeQuietly(connection);
            }
        }
    }

    public void executeItemsAllRepoDSR(List<COUNTER_Dataset_Usage> reportItems,
            String itemIdentifier, Date beginDate,
            Date endDate, List<String> metricType, String granularity) throws Exception {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
        String beginDateStr = postgresFormat.format(beginDate);
        String endDateStr = postgresFormat.format(endDate);

        Connection connection = null;
        PreparedStatement st = null;
        PreparedStatement st1 = null;
        ResultSet rs = null;
        ResultSet rs1 = null;

        try {
            connection = usageStatsDB.getConnection();

            if (granularity.equalsIgnoreCase("totals")) {

                st = connection.prepareStatement("WITH tpd as (select id, concat(type,'#-#',pid) type_id from " + statsDB + ".result_pids) "
                        + "SELECT rc.type, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                        + "us.result_id as resultid, rs.title as item, rs.year as yop, group_concat(distinct tpd.type_id,'#!#') as oid, ds.name, "
                        + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations, "
                        + "case when us.source='Datacite' then dp.access_method else 'regular' end as access_method "
                        + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us, " + statsDB + ".result_classifications rc, " + statsDB + ".result rs, "
                        + statsDB + ".result_oids ro, tpd, " + statsDB + ".datasource ds, openaire_prod_datacite_usage_stats.datasetsperformance_nonarray_view dp "
                        + "WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=ds.id AND us.result_id=? AND rc.id=us.result_id "
                        + "AND us.result_id=rs.id AND ro.id=us.result_id AND tpd.id=us.result_id AND rc.type='Dataset' AND dp.ds_type=ro.oid "
                        + "GROUP BY us.source, ro.id, rc.type, rs.title, us.result_id, rs.year, ds.name,dp.access_method ORDER by rc.type ASC;");
                st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                        + "SELECT type, total_item_requests, total_item_investigations, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid,name, unique_item_requests,unique_item_investigations, 'Regular' as access_method "
                        + "FROM (SELECT us.repository_id repo, rc.type type, ds.name, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                        + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                        + "sum(us.unique_item_requests) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                        + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, " + statsDB + ".result rs, "
                        + statsDB + ".datasource ds WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.result_id=? "
                        + "AND rc.type='Dataset' AND ds.id=us.repository_id GROUP BY rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id, ds.name) tmp, tpd "
                        + "WHERE tpd.id=resultid GROUP BY name, type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations limit 10;");
                st.setString(1, beginDateStr);
                st.setString(2, endDateStr);
                st.setString(3, itemIdentifier);
//                        + "UNION "
                st1 = connection.prepareStatement("SELECT 'Dataset' as type, sum(case when dp.metric_type='total-dataset-requests' then count else 0 end) as downloads, "
                        + "sum(case when dp.metric_type='total-dataset-investigations' then count else 0 end) as views, "
                        + "rp.id as resultid,dp.ds_title as item, cast(dp.yop as Int) as yop, concat('Digital Object Identifier#-#',dp.ds_type) as oid, dp.platform, "
                        + "sum(case when dp.metric_type='unique-dataset-requests' then count else 0 end) as uniquedownloads, "
                        + "sum(case when dp.metric_type='unique-dataset-investigations' then count else 0 end) as uniqueviews, "
                        + "dp.access_method access_method FROM openaire_prod_datacite_usage_stats.datasetsperformance_nonarray_view dp, "
                        + statsDB + ".result_pids rp, " + statsDB + ".datasource ds "
                        + "WHERE dp.period_from>=? AND dp.period_end<=? and rp.pid=ds_type AND rp.id=? "
                        + "AND ds.name=dp.platform GROUP BY dp.ds_title, dp.yop, dp.platform, dp.access_method, dp.ds_type,rp.id limit 10");

                st1.setString(1, report_dateFormat.format(beginDate));
                st1.setString(2, report_dateFormat.format(endDate));
                st1.setString(3, itemIdentifier);

                COUNTER_Dataset_Usage reportItem = null;
                boolean[] metricTypeValue = {false, false, false, false};

                if (metricType.contains("Total_Dataset_Requests")) {
                    metricTypeValue[0] = true;
                }
                if (metricType.contains("Total_Dataset_Investigations")) {
                    metricTypeValue[1] = true;
                }
                if (metricType.contains("Unique_Dataset_Requests")) {
                    metricTypeValue[2] = true;
                }
                if (metricType.contains("Unique_Dataset_Investigations")) {
                    metricTypeValue[3] = true;
                }

                rs = st.executeQuery();
                while (rs.next()) {
                    // String platform = getInstitutionName(rs.getString(8));
                    reportItem = new COUNTER_Dataset_Usage(rs.getString(5), "OpenAIRE", rs.getString(8), rs.getString(1), Integer.toString(rs.getInt(6)), rs.getString(11));
                    String[] identifiersAll = rs.getString(7).split("#!#");
                    for (int i = 0; i < identifiersAll.length; i++) {
                        String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                        reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                    }

                    reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                            metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                    reportItems.add(reportItem);
                }
                rs1 = st1.executeQuery();
                while (rs1.next()) {
                    // String platform = getInstitutionName(rs.getString(8));
                    reportItem = new COUNTER_Dataset_Usage(rs1.getString(5), "OpenAIRE", rs1.getString(8), rs1.getString(1), Integer.toString(rs1.getInt(6)), rs1.getString(11));
                    String[] identifiersAll = rs1.getString(7).split("#!#");
                    for (int i = 0; i < identifiersAll.length; i++) {
                        String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                        reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                    }

                    reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                            metricTypeValue[0] == true ? rs1.getString(2) : null, metricTypeValue[1] == true ? rs1.getString(3) : null, metricTypeValue[2] == true ? rs1.getString(9) : null, metricTypeValue[3] == true ? rs1.getString(10) : null));
                    reportItems.add(reportItem);
                }
            } else if (granularity.equalsIgnoreCase("monthly")) {

//                st = connection.prepareStatement("WITH tpd as (select id, concat(type,'#-#',pid) type_id from " + statsDB + ".result_pids) "
//                        + "SELECT rc.type, us.`date`, sum(us.downloads) as downloads, sum(us.views) as views, "
//                        + "us.result_id as resultid, rs.title as item, rs.year as yop, group_concat(distinct tpd.type_id,'#!#') as oid, ds.name, "
//                        + "count(case when us.downloads >0 then 1 else null end) as uniquedownloads, count(case when us.views >0 then 1 else null end) as uniqueviews, "
//                        + "case when us.source='Datacite' then dp.access_method else 'regular' end as access_method "
//                        + "FROM " + usagestatsImpalaDB + ".usage_stats us, " + statsDB + ".result_classifications rc, " + statsDB + ".result rs, "
//                        + statsDB + ".result_oids ro, tpd, " + statsDB + ".datasource ds, datasetsusagestats_20210312a.datasetsperformance dp "
//                        + "WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=ds.id AND us.result_id=? AND rc.id=us.result_id "
//                        + "AND us.result_id=rs.id AND ro.id=us.result_id AND tpd.id=us.result_id AND rc.type='Dataset' AND dp.ds_type=ro.oid "
//                        + "GROUP BY us.source, ro.id, rc.type, rs.title, us.result_id, us.`date`, rs.year, ds.name, dp.access_method ORDER by rc.type, us.`date` ASC;");
                st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
                        + "SELECT type, `date`, total_item_requests, total_item_investigations, resultid, item, yop, group_concat(distinct tpd.type_id,'#!#') as oid, unique_item_requests,unique_item_investigations, name, 'Regular' as access_method "
                        + "FROM (SELECT us.repository_id repo, us.`date` as `date`, rc.type type,ds.name, sum(us.total_item_requests) as total_item_requests, sum(us.total_item_investigations) as total_item_investigations, "
                        + "us.result_id as resultid, rs.title as item, rs.year as yop, "
                        + "sum(us.unique_item_requests)) as unique_item_requests, sum(us.unique_item_investigations) as unique_item_investigations "
                        + "FROM " + usagestatsImpalaDB + ".counter_r5_stats_with_metrics us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, openaire_prod_stats.result rs, "
                        + statsDB + ".datasource ds WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND ds.id=us.repository_id AND us.result_id=? "
                        + "AND rc.type='Dataset' GROUP BY ds.name, us.repository_id, rc.type, us.`date`, rs.title, us.result_id, rs.title, rs.year, us.`date`) tmp, tpd "
                        + "WHERE tpd.id=resultid GROUP BY name,`date`, type,resultid,item,yop,total_item_requests,total_item_investigations,unique_item_requests,unique_item_investigations ORDER BY `date` ASC limit 10;");
                st.setString(1, beginDateStr);
                st.setString(2, endDateStr);
                st.setString(3, itemIdentifier);

                st1 = connection.prepareStatement("SELECT 'Dataset' as type, dp.period_from, sum(case when dp.metric_type='total-dataset-requests' then count else 0 end) as downloads, "
                        + "sum(case when dp.metric_type='total-dataset-investigations' then count else 0 end) as views, "
                        + "rp.id as resultid,dp.ds_title as item, dp.yop as yop, concat('Digital Object Identifier#-#',dp.ds_type) as oid, "
                        + "sum(case when dp.metric_type='unique-dataset-requests' then count else 0 end) as uniquedownloads, "
                        + "sum(case when dp.metric_type='unique-dataset-investigations' then count else 0 end) as uniqueviews, dp.platform, "
                        + "dp.access_method access_method FROM openaire_prod_datacite_usage_stats.datasetsperformance_nonarray_view dp, "
                        + statsDB + ".result_pids rp, " + statsDB + ".datasource ds "
                        + "WHERE dp.period_from>=? AND dp.period_end<=? and rp.pid=ds_type and rp.id=? and ds.name=dp.platform "
                        + "GROUP BY dp.ds_title, dp.yop, dp.platform, dp.access_method, dp.ds_type,rp.id, dp.period_from limit 10");
                st1.setString(1, report_dateFormat.format(beginDate));
                st1.setString(2, report_dateFormat.format(endDate));
                st1.setString(3, itemIdentifier);

                rs = st.executeQuery();
                String result = "";
                String lastDate = "";
                COUNTER_Dataset_Usage reportItem = null;
                boolean[] metricTypeValue = {false, false, false, false};

                if (metricType.contains("Total_Dataset_Requests")) {
                    metricTypeValue[0] = true;
                }
                if (metricType.contains("Total_Dataset_Investigations")) {
                    metricTypeValue[1] = true;
                }
                if (metricType.contains("Unique_Dataset_Requests")) {
                    metricTypeValue[2] = true;
                }
                if (metricType.contains("Unique_Dataset_Investigations")) {
                    metricTypeValue[3] = true;
                }

                int ft_total = 0;
                int abstr = 0;

                Calendar endCal = Calendar.getInstance();
                endCal.setTime(postgresFormat.parse(endDateStr));
                endCal.add(Calendar.MONTH, 1);
                Date endDateForZeros = endCal.getTime();

                lastDate = beginDateStr;
                String datatype = "";

                while (rs.next()) {
                    Calendar endC = Calendar.getInstance();
                    endC.setTime(postgresFormat.parse(rs.getString(2)));
                    endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                    if (!datatype.equals(rs.getString(1))) {
//                                if (reportItem != null) {
//                                    reportItems.add(reportItem);
//                                }
//                                reportItem = new COUNTER_Platform_Usage("", "OpenAIRE", rs.getString(1), "Regular", "");
                        datatype = rs.getString(1);
                    }
                    reportItem = new COUNTER_Dataset_Usage(rs.getString(6), "OpenAIRE", rs.getString(11), rs.getString(1), Integer.toString(rs.getInt(7)), rs.getString(12));
                    String[] identifiersAll = rs.getString(8).split("#!#");
                    for (int i = 0; i < identifiersAll.length; i++) {
                        String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                        reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                    }

                    reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                            metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                    reportItems.add(reportItem);

                    endC.setTime(postgresFormat.parse(rs.getString(2)));
                    endC.add(Calendar.MONTH, 1);
                    lastDate = postgresFormat.format(endC.getTime());
                    //if (reportItem != null) {
                    //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                }
                rs1 = st1.executeQuery();
                while (rs1.next()) {
                    Calendar endC = Calendar.getInstance();
                    endC.setTime(report_dateFormat.parse(rs1.getString(2)));
                    endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                    if (!datatype.equals(rs1.getString(1))) {
//                                if (reportItem != null) {
//                                    reportItems.add(reportItem);
//                                }
//                                reportItem = new COUNTER_Platform_Usage("", "OpenAIRE", rs.getString(1), "Regular", "");
                        datatype = rs1.getString(1);
                    }
                    reportItem = new COUNTER_Dataset_Usage(rs1.getString(6), "OpenAIRE", rs1.getString(11), rs1.getString(1), Integer.toString(rs1.getInt(7)), rs1.getString(12));
                    String[] identifiersAll = rs1.getString(8).split("#!#");
                    for (int i = 0; i < identifiersAll.length; i++) {
                        String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                        reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                    }

                    reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(report_dateFormat.parse(rs1.getString(2))), report_dateFormat.format(endC.getTime()),
                            metricTypeValue[0] == true ? rs1.getString(3) : null, metricTypeValue[1] == true ? rs1.getString(4) : null, metricTypeValue[2] == true ? rs1.getString(9) : null, metricTypeValue[3] == true ? rs1.getString(10) : null));
                    reportItems.add(reportItem);

                    endC.setTime(report_dateFormat.parse(rs1.getString(2)));
                    endC.add(Calendar.MONTH, 1);
                    lastDate = report_dateFormat.format(endC.getTime());
                    //if (reportItem != null) {
                    //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                }

            }
            /*
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "query", st.toString());
            jedis.put(redis_key, "result", toJson(reportItems));
            jedis.put(redis_key, "fetchMode", "3");
             */
        } catch (Exception e) {
            log.error("Batch Item Report failed: ", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
    }

    public void executeBatchItemsDSRDatacite(List<COUNTER_Dataset_Usage> reportItems,
            String repositoryIdentifier, String itemIdentifier, Date beginDate,
            Date endDate, List<String> metricType, String granularity) throws Exception {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
        String beginDateStr = report_dateFormat.format(beginDate);
        String endDateStr = report_dateFormat.format(endDate);

        Connection connection = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String platform = getInstitutionName(repositoryIdentifier);

        if (itemIdentifier.equals("")) {
            try {
                connection = usageStatsDB.getConnection();

                if (granularity.equalsIgnoreCase("totals")) {

//                    st = connection.prepareStatement("WITH tpd as (select id, concat(type,'#-#',pid) type_id from " + statsDB + ".result_pids) "
//                            + "SELECT rc.type, sum(us.downloads) as downloads, sum(us.views) as views, "
//                            + "us.result_id as resultid, rs.title as item, rs.year as yop, group_concat(distinct tpd.type_id,'#!#') as oid, "
//                            + "count(case when us.downloads >0 then 1 else null end) as uniquedownloads, count(case when us.views >0 then 1 else null end) as uniqueviews,  "
//                            + "case when us.source='Datacite' then dp.access_method else 'regular' end as access_method "
//                            + "FROM " + usagestatsImpalaDB + ".usage_stats us, " + statsDB + ".result_classifications rc, " + statsDB + ".result rs, "
//                            + statsDB + ".result_oids ro, tpd, datasetsusagestats_20210312a.datasetsperformance dp "
//                            + "WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND rc.id=us.result_id "
//                            + "AND us.result_id=rs.id AND ro.id=us.result_id AND tpd.id=us.result_id AND rc.type='Dataset' AND dp.ds_type=ro.oid "
//                            + "GROUP BY us.source, ro.id, rc.type, rs.title, us.result_id, rs.year, dp.access_method ORDER by rc.type ASC;");
                    st = connection.prepareStatement("SELECT 'Dataset' as type, sum(case when dp.metric_type='total-dataset-requests' then count else 0 end) as downloads, "
                            + "sum(case when dp.metric_type='total-dataset-investigations' then count else 0 end) as views, "
                            + "rp.id as resultid,dp.ds_title as item, dp.yop as yop, concat('Digital Object Identifier#-#',dp.ds_type) as oid, "
                            + "sum(case when dp.metric_type='unique-dataset-requests' then count else 0 end) as uniquedownloads, "
                            + "sum(case when dp.metric_type='unique-dataset-investigations' then count else 0 end) as uniqueviews, "
                            + "dp.access_method access_method, dp.platform FROM openaire_prod_datacite_usage_stats.datasetsperformance_nonarray_view dp, "
                            + statsDB + ".result_pids rp, " + statsDB + ".datasource ds "
                            + "WHERE dp.period_from>=? AND dp.period_end<=? and rp.pid=ds_type and ds.id=? and ds.name=dp.platform "
                            + "GROUP BY dp.ds_title, dp.yop, dp.platform, dp.access_method, dp.ds_type,rp.id limit 10;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);

                    rs = st.executeQuery();
                    COUNTER_Dataset_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};

                    if (metricType.contains("Total_Dataset_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Dataset_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Investigations")) {
                        metricTypeValue[3] = true;
                    }
                    while (rs.next()) {
                        reportItem = new COUNTER_Dataset_Usage(rs.getString(5), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(6)), rs.getString(10));
                        String[] identifiersAll = rs.getString(7).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }

                        reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(8) : null, metricTypeValue[3] == true ? rs.getString(9) : null));
                        reportItems.add(reportItem);
                    }
                    //}
                } else if (granularity.equalsIgnoreCase("monthly")) {
//                    st = connection.prepareStatement("WITH tpd as (select id, concat(type,'#-#',pid) type_id from " + statsDB + ".result_pids) "
//                            + "SELECT rc.type, us.`date`, sum(us.downloads) as downloads, sum(us.views) as views, "
//                            + "us.result_id as resultid, rs.title as item, rs.year as yop, group_concat(distinct tpd.type_id,'#!#') as oid, "
//                            + "count(case when us.downloads >0 then 1 else null end) as uniquedownloads, count(case when us.views >0 then 1 else null end) as uniqueviews, "
//                            + "case when us.source='Datacite' then dp.access_method else 'regular' end as access_method "
//                            + "FROM " + usagestatsImpalaDB + ".usage_stats us, " + statsDB + ".result_classifications rc, " + statsDB + ".result rs, "
//                            + statsDB + ".result_oids ro, tpd, datasetsusagestats_20210312a.datasetsperformance dp  "
//                            + "WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND rc.id=us.result_id "
//                            + "AND us.result_id=rs.id AND ro.id=us.result_id AND tpd.id=us.result_id AND rc.type='Dataset' AND dp.ds_type=ro.oid "
//                            + "GROUP BY us.source, ro.id, rc.type, rs.title, us.result_id, us.`date`, rs.year, dp.access_method ORDER by rc.type, us.`date` ASC;");

                    st = connection.prepareStatement("SELECT 'Dataset' as type, dp.period_from, sum(case when dp.metric_type='total-dataset-requests' then count else 0 end) as downloads, "
                            + "sum(case when dp.metric_type='total-dataset-investigations' then count else 0 end) as views, "
                            + "rp.id as resultid,dp.ds_title as item, dp.yop as yop, concat('Digital Object Identifier#-#',dp.ds_type) as oid, "
                            + "sum(case when dp.metric_type='unique-dataset-requests' then count else 0 end) as uniquedownloads, "
                            + "sum(case when dp.metric_type='unique-dataset-investigations' then count else 0 end) as uniqueviews, "
                            + "dp.access_method access_method, dp.platform FROM openaire_prod_datacite_usage_stats.datasetsperformance_nonarray_view dp, "
                            + statsDB + ".result_pids rp, " + statsDB + ".datasource ds "
                            + "WHERE dp.period_from>=? AND dp.period_end<=? and rp.pid=ds_type and ds.id=? and ds.name=dp.platform "
                            + "GROUP BY dp.ds_title, dp.yop, dp.platform, dp.access_method, dp.ds_type,rp.id, dp.period_from limit 10;");

                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);
                    rs = st.executeQuery();

                    String result = "";
                    String lastDate = "";
                    COUNTER_Dataset_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};

                    if (metricType.contains("Total_Dataset_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Dataset_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    int ft_total = 0;
                    int abstr = 0;

                    Calendar endCal = Calendar.getInstance();
                    endCal.setTime(report_dateFormat.parse(endDateStr));
                    endCal.add(Calendar.MONTH, 1);
                    Date endDateForZeros = endCal.getTime();

                    lastDate = beginDateStr;
                    String datatype = "";
                    List<COUNTER_Item_Identifiers> identifiers = null;

                    while (rs.next()) {
                        Calendar endC = Calendar.getInstance();
                        endC.setTime(report_dateFormat.parse(rs.getString(2)));
                        endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                        if (!datatype.equals(rs.getString(1))) {
//                                if (reportItem != null) {
//                                    reportItems.add(reportItem);
//                                }
//                                reportItem = new COUNTER_Platform_Usage("", "OpenAIRE", rs.getString(1), "Regular", "");
                            datatype = rs.getString(1);
                        }

                        reportItem = new COUNTER_Dataset_Usage(rs.getString(6), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(7)), rs.getString(11));
                        String[] identifiersAll = rs.getString(8).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }
                        reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(report_dateFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                        reportItems.add(reportItem);

                        endC.setTime(report_dateFormat.parse(rs.getString(2)));
                        endC.add(Calendar.MONTH, 1);
                        lastDate = report_dateFormat.format(endC.getTime());
                        //if (reportItem != null) {
                        //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                    }
                }
                /*
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "query", st.toString());
            jedis.put(redis_key, "result", toJson(reportItems));
            jedis.put(redis_key, "fetchMode", "3");
                 */
            } catch (Exception e) {
                log.error("Batch Item Report failed: ", e);
            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(st);
                DbUtils.closeQuietly(connection);
            }
        } else {
            try {
                connection = usageStatsDB.getConnection();

                if (granularity.equalsIgnoreCase("totals")) {
//                    st = connection.prepareStatement("WITH tpd as (select id, concat(type,'#-#',pid) type_id from " + statsDB + ".result_pids) "
//                            + "SELECT rc.type, sum(us.downloads) as downloads, sum(us.views) as views, "
//                            + "us.result_id as resultid, rs.title as item, rs.year as yop, group_concat(distinct tpd.type_id,'#!#') as oid, "
//                            + "count(case when us.downloads >0 then 1 else null end) as uniquedownloads, count(case when us.views >0 then 1 else null end) as uniqueviews,"
//                            + "case when us.source='Datacite' then dp.access_method else 'regular' end as access_method  "
//                            + "FROM " + usagestatsImpalaDB + ".usage_stats us, " + statsDB + ".result_classifications rc, " + statsDB + ".result rs, "
//                            + statsDB + ".result_oids ro, tpd, datasetsusagestats_20210312a.datasetsperformance dp "
//                            + "WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND us.result_id=? AND rc.id=us.result_id "
//                            + "AND us.result_id=rs.id AND ro.id=us.result_id AND tpd.id=us.result_id AND rc.type='Dataset' AND dp.ds_type=ro.oid "
//                            + "GROUP BY us.source, ro.id, rc.type, rs.title, us.result_id, rs.year, dp.access_method ORDER by rc.type ASC;");
                    st = connection.prepareStatement("SELECT 'Dataset' as type, sum(case when dp.metric_type='total-dataset-requests' then count else 0 end) as downloads, "
                            + "sum(case when dp.metric_type='total-dataset-investigations' then count else 0 end) as views, "
                            + "rp.id as resultid,dp.ds_title as item, dp.yop as yop, concat('Digital Object Identifier#-#',dp.ds_type) as oid, "
                            + "sum(case when dp.metric_type='unique-dataset-requests' then count else 0 end) as uniquedownloads, "
                            + "sum(case when dp.metric_type='unique-dataset-investigations' then count else 0 end) as uniqueviews, "
                            + "dp.access_method access_method, dp.platform FROM openaire_prod_datacite_usage_stats.datasetsperformance_nonarray_view dp, "
                            + statsDB + ".result_pids rp, " + statsDB + ".datasource ds "
                            + "WHERE dp.period_from>=? AND dp.period_end<=? and rp.pid=ds_type and ds.id=? AND rp.id=? and ds.name=dp.platform "
                            + "GROUP BY dp.ds_title, dp.yop, dp.platform, dp.access_method, dp.ds_type,rp.id limit 10;");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);
                    st.setString(4, itemIdentifier);

                    rs = st.executeQuery();
                    COUNTER_Dataset_Usage reportItem = null;
                    boolean[] metricTypeValue = {false, false, false, false};
                    if (metricType.contains("Total_Dataset_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Dataset_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    while (rs.next()) {
                        reportItem = new COUNTER_Dataset_Usage(rs.getString(5), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(6)), rs.getString(10));
                        String[] identifiersAll = rs.getString(7).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }
                        reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
                                metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(8) : null, metricTypeValue[3] == true ? rs.getString(9) : null));
                        reportItems.add(reportItem);
                    }
                } else if (granularity.equalsIgnoreCase("monthly")) {

//                    st = connection.prepareStatement("WITH tpd as (select id, concat(type,'#-#',pid) type_id from " + statsDB + ".result_pids) "
//                            + "SELECT rc.type, us.`date`, sum(us.downloads) as downloads, sum(us.views) as views, "
//                            + "us.result_id as resultid, rs.title as item, rs.year as yop, group_concat(distinct tpd.type_id,'#!#') as oid, "
//                            + "count(case when us.downloads >0 then 1 else null end) as uniquedownloads, count(case when us.views >0 then 1 else null end) as uniqueviews,  "
//                            + "case when us.source='Datacite' then dp.access_method else 'regular' end as access_method "
//                            + "FROM " + usagestatsImpalaDB + ".usage_stats us, " + statsDB + ".result_classifications rc, " + statsDB + ".result rs, "
//                            + statsDB + ".result_oids ro, tpd, datasetsusagestats_20210312a.datasetsperformance dp  "
//                            + "WHERE us.`date`>=? AND us.`date`<=? AND us.repository_id=? AND us.result_id=? AND rc.id=us.result_id "
//                            + "AND us.result_id=rs.id AND ro.id=us.result_id AND tpd.id=us.result_id AND rc.type='Dataset' AND dp.ds_type=ro.oid "
//                            + "GROUP BY us.source, ro.id, rc.type, rs.title, us.result_id, us.`date`, rs.year, dp.access_method ORDER by rc.type, us.`date` ASC;");
                    st = connection.prepareStatement("SELECT 'Dataset' as type, dp.period_from, sum(case when dp.metric_type='total-dataset-requests' then count else 0 end) as downloads, "
                            + "sum(case when dp.metric_type='total-dataset-investigations' then count else 0 end) as views, "
                            + "rp.id as resultid,dp.ds_title as item, dp.yop as yop, concat('Digital Object Identifier#-#',dp.ds_type) as oid, "
                            + "sum(case when dp.metric_type='unique-dataset-requests' then count else 0 end) as uniquedownloads, "
                            + "sum(case when dp.metric_type='unique-dataset-investigations' then count else 0 end) as uniqueviews, "
                            + "dp.access_method access_method, dp.platform FROM openaire_prod_datacite_usage_stats.datasetsperformance_nonarray_view dp, "
                            + statsDB + ".result_pids rp, " + statsDB + ".datasource ds "
                            + "WHERE dp.period_from>=? AND dp.period_end<=? and rp.pid=ds_type and ds.id=? AND rp.id=? and ds.name=dp.platform "
                            + "GROUP BY dp.ds_title, dp.yop, dp.platform, dp.access_method, dp.ds_type,rp.id, dp.period_from limit 10");
                    st.setString(1, beginDateStr);
                    st.setString(2, endDateStr);
                    st.setString(3, repositoryIdentifier);
                    st.setString(4, itemIdentifier);

                    rs = st.executeQuery();
                    String result = "";
                    String lastDate = "";
                    COUNTER_Dataset_Usage reportItem = null;

                    boolean[] metricTypeValue = {false, false, false, false};

                    if (metricType.contains("Total_Dataset_Requests")) {
                        metricTypeValue[0] = true;
                    }
                    if (metricType.contains("Total_Dataset_Investigations")) {
                        metricTypeValue[1] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Requests")) {
                        metricTypeValue[2] = true;
                    }
                    if (metricType.contains("Unique_Dataset_Investigations")) {
                        metricTypeValue[3] = true;
                    }

                    int ft_total = 0;
                    int abstr = 0;

                    Calendar endCal = Calendar.getInstance();
                    endCal.setTime(report_dateFormat.parse(endDateStr));
                    endCal.add(Calendar.MONTH, 1);
                    Date endDateForZeros = endCal.getTime();

                    lastDate = beginDateStr;
                    String datatype = "";

                    while (rs.next()) {
                        Calendar endC = Calendar.getInstance();
                        endC.setTime(report_dateFormat.parse(rs.getString(2)));
                        endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
//                            //iterate on data types
                        if (!datatype.equals(rs.getString(1))) {
                            datatype = rs.getString(1);
                        }

                        reportItem = new COUNTER_Dataset_Usage(rs.getString(6), "OpenAIRE", platform, rs.getString(1), Integer.toString(rs.getInt(7)), rs.getString(11));
                        String[] identifiersAll = rs.getString(8).split("#!#");
                        for (int i = 0; i < identifiersAll.length; i++) {
                            String[] typeIdentifierArray = identifiersAll[i].split("#-#");
                            reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
                        }
                        reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(report_dateFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
                                metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
                        reportItems.add(reportItem);

                        endC.setTime(report_dateFormat.parse(rs.getString(2)));
                        endC.add(Calendar.MONTH, 1);
                        lastDate = report_dateFormat.format(endC.getTime());
                        //if (reportItem != null) {
                        //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
                    }
                }
                /*
            jedis.put(redis_key, "persistent", "false");
            jedis.put(redis_key, "query", st.toString());
            jedis.put(redis_key, "result", toJson(reportItems));
            jedis.put(redis_key, "fetchMode", "3");
                 */
            } catch (Exception e) {
                log.error("Batch Item Report failed: ", e);
            } finally {
                DbUtils.closeQuietly(rs);
                DbUtils.closeQuietly(st);
                DbUtils.closeQuietly(connection);
            }
        }
    }

//    public void executeItemsAllRepoDSRDatacite(List<COUNTER_Dataset_Usage> reportItems,
//            String itemIdentifier, Date beginDate,
//            Date endDate, List<String> metricType, String granularity) throws Exception {
//        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        SimpleDateFormat postgresFormat = new SimpleDateFormat("yyyy/MM");
//        String beginDateStr = postgresFormat.format(beginDate);
//        String endDateStr = postgresFormat.format(endDate);
//
//        Connection connection = null;
//        PreparedStatement st = null;
//        ResultSet rs = null;
//
//        try {
//            connection = usageStatsDB.getConnection();
//
//            if (granularity.equalsIgnoreCase("totals")) {
//
////                st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
////                        + "SELECT type, downloads, views, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, uniquedownloads,uniqueviews, 'Regular' as access_method "
////                        + "FROM (SELECT us.repository_id repo, rc.type type, sum(us.downloads) as downloads, sum(us.views) as views, "
////                        + "us.result_id as resultid, rs.title as item, rs.year as yop, "
////                        + "count(case when us.downloads >0 then 1 else null end) as uniquedownloads, count(case when us.views >0 then 1 else null end) as uniqueviews "
////                        + "FROM " + usagestatsImpalaDB + ".usage_stats us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, openaire_prod_stats.result rs "
////                        + "WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.result_id=? "
////                        + "AND rc.type='Dataset' GROUP BY rc.type, rs.title, us.result_id, rs.title, rs.year, us.repository_id) tmp, tpd "
////                        + "WHERE tpd.id=resultid GROUP BY repo, type,resultid,item,yop,downloads,views,uniquedownloads,uniqueviews");
//                st = connection.prepareStatement("SELECT 'Dataset' as type, sum(case when dp.metric_type='total-dataset-requests' then count else 0 end) as downloads, "
//                        + "sum(case when dp.metric_type='total-dataset-investigations' then count else 0 end) as views, "
//                        + "rp.id as resultid,dp.ds_title as item, dp.yop as yop, concat('Digital Object Identifier#-#',dp.ds_type) as oid, "
//                        + "sum(case when dp.metric_type='unique-dataset-requests' then count else 0 end) as uniquedownloads, "
//                        + "sum(case when dp.metric_type='unique-dataset-investigations' then count else 0 end) as uniqueviews, "
//                        + "dp.access_method access_method, dp.platform FROM datasetsusagestats_20210312a.datasetsperformance dp, "
//                        + statsDB + ".result_pids rp, " + statsDB + ".datasource ds "
//                        + "WHERE dp.period_from>=? AND dp.period_end<=? and rp.pid=ds_type AND rp.id=? and ds.name=dp.platform "
//                        + "GROUP BY dp.ds_title, dp.yop, dp.platform, dp.access_method, dp.ds_type,rp.id");
//                st.setString(1, beginDateStr);
//                st.setString(2, endDateStr);
//                st.setString(3, itemIdentifier);
//
//                rs = st.executeQuery();
//                COUNTER_Dataset_Usage reportItem = null;
//                boolean[] metricTypeValue = {false, false, false, false};
//
//                if (metricType.contains("Total_Dataset_Requests")) {
//                    metricTypeValue[0] = true;
//                }
//                if (metricType.contains("Total_Dataset_Investigations")) {
//                    metricTypeValue[1] = true;
//                }
//                if (metricType.contains("Unique_Dataset_Requests")) {
//                    metricTypeValue[2] = true;
//                }
//                if (metricType.contains("Unique_Dataset_Investigations")) {
//                    metricTypeValue[3] = true;
//                }
//
//                while (rs.next()) {
//                    String platform = getInstitutionName(rs.getString(8));
//                    reportItem = new COUNTER_Dataset_Usage(rs.getString(5), "OpenAIRE", rs.getString(8), rs.getString(1), Integer.toString(rs.getInt(6)), rs.getString(11));
//                    String[] identifiersAll = rs.getString(7).split("#!#");
//                    for (int i = 0; i < identifiersAll.length; i++) {
//                        String[] typeIdentifierArray = identifiersAll[i].split("#-#");
//                        reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
//                    }
//
//                    reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(beginDate), report_dateFormat.format(endDate),
//                            metricTypeValue[0] == true ? rs.getString(2) : null, metricTypeValue[1] == true ? rs.getString(3) : null, metricTypeValue[2] == true ? rs.getString(9) : null, metricTypeValue[3] == true ? rs.getString(10) : null));
//                    reportItems.add(reportItem);
//                }
//
//                //}
//            } else if (granularity.equalsIgnoreCase("monthly")) {
//
////                st = connection.prepareStatement("WITH tpd as (SELECT distinct id, concat(type,'#-#',pid) type_id FROM " + statsDB + ".result_pids) "
////                        + "SELECT type, `date`, downloads, views, resultid, item,yop,group_concat(distinct tpd.type_id,'#!#') as oid, uniquedownloads,uniqueviews, 'Regular' as access_method "
////                        + "FROM (SELECT us.repository_id repo, us.`date` as `date`, rc.type type, sum(us.downloads) as downloads, sum(us.views) as views, "
////                        + "us.result_id as resultid, rs.title as item, rs.year as yop, "
////                        + "count(case when us.downloads >0 then 1 else null end) as uniquedownloads, count(case when us.views >0 then 1 else null end) as uniqueviews "
////                        + "FROM " + usagestatsImpalaDB + ".usage_stats us,(SELECT distinct id, type FROM " + statsDB + ".result_classifications) rc, openaire_prod_stats.result rs "
////                        + "WHERE us.`date`>=? AND us.`date`<=? AND rc.id=us.result_id AND us.result_id=rs.id AND us.result_id=? "
////                        + "AND rc.type='Dataset' GROUP BY rc.type, us.`date`, rs.title, us.result_id, rs.title, rs.year, us.`date`) tmp, tpd "
////                        + "WHERE tpd.id=resultid GROUP BY repo,`date`, type,resultid,item,yop,downloads,views,uniquedownloads,uniqueviews ORDER BY `date` ASC;");
//                st = connection.prepareStatement("SELECT 'Dataset' as type, dp.period_from, sum(case when dp.metric_type='total-dataset-requests' then count else 0 end) as downloads, "
//                        + "sum(case when dp.metric_type='total-dataset-investigations' then count else 0 end) as views, "
//                        + "rp.id as resultid,dp.ds_title as item, dp.yop as yop, concat('Digital Object Identifier#-#',dp.ds_type) as oid, "
//                        + "sum(case when dp.metric_type='unique-dataset-requests' then count else 0 end) as uniquedownloads, "
//                        + "sum(case when dp.metric_type='unique-dataset-investigations' then count else 0 end) as uniqueviews, "
//                        + "dp.access_method access_method, dp.platform FROM datasetsusagestats_20210312a.datasetsperformance dp, "
//                        + statsDB + ".result_pids rp, " + statsDB + ".datasource ds "
//                        + "WHERE dp.period_from>=? AND dp.period_end<=? and rp.pid=ds_type and rp.id=? and ds.name=dp.platform "
//                        + "GROUP BY dp.ds_title, dp.yop, dp.platform, dp.access_method, dp.ds_type,rp.id, dp.period_from");
//                st.setString(1, beginDateStr);
//                st.setString(2, endDateStr);
//                st.setString(3, itemIdentifier);
//
//                rs = st.executeQuery();
//                String result = "";
//                String lastDate = "";
//                COUNTER_Dataset_Usage reportItem = null;
//                boolean[] metricTypeValue = {false, false, false, false};
//
//                if (metricType.contains("Total_Dataset_Requests")) {
//                    metricTypeValue[0] = true;
//                }
//                if (metricType.contains("Total_Dataset_Investigations")) {
//                    metricTypeValue[1] = true;
//                }
//                if (metricType.contains("Unique_Dataset_Requests")) {
//                    metricTypeValue[2] = true;
//                }
//                if (metricType.contains("Unique_Dataset_Investigations")) {
//                    metricTypeValue[3] = true;
//                }
//
//                int ft_total = 0;
//                int abstr = 0;
//
//                Calendar endCal = Calendar.getInstance();
//                endCal.setTime(postgresFormat.parse(endDateStr));
//                endCal.add(Calendar.MONTH, 1);
//                Date endDateForZeros = endCal.getTime();
//
//                lastDate = beginDateStr;
//                String datatype = "";
//
//                while (rs.next()) {
//                    Calendar endC = Calendar.getInstance();
//                    endC.setTime(postgresFormat.parse(rs.getString(2)));
//                    endC.set(Calendar.DATE, endC.getActualMaximum(Calendar.DATE));
////                            //iterate on data types
//                    if (!datatype.equals(rs.getString(1))) {
////                                if (reportItem != null) {
////                                    reportItems.add(reportItem);
////                                }
////                                reportItem = new COUNTER_Platform_Usage("", "OpenAIRE", rs.getString(1), "Regular", "");
//                        datatype = rs.getString(1);
//                    }
//                    reportItem = new COUNTER_Dataset_Usage(rs.getString(6), "OpenAIRE", rs.getString(9), rs.getString(1), Integer.toString(rs.getInt(7)), rs.getString(12));
//                    String[] identifiersAll = rs.getString(8).split("#!#");
//                    for (int i = 0; i < identifiersAll.length; i++) {
//                        String[] typeIdentifierArray = identifiersAll[i].split("#-#");
//                        reportItem.addIdentifier(new COUNTER_Dataset_Identifiers(typeIdentifierArray[0], typeIdentifierArray[1]));
//                    }
//
//                    reportItem.addPerformance(new COUNTER_Dataset_Performance(report_dateFormat.format(postgresFormat.parse(rs.getString(2))), report_dateFormat.format(endC.getTime()),
//                            metricTypeValue[0] == true ? rs.getString(3) : null, metricTypeValue[1] == true ? rs.getString(4) : null, metricTypeValue[2] == true ? rs.getString(10) : null, metricTypeValue[3] == true ? rs.getString(11) : null));
//                    reportItems.add(reportItem);
//
//                    endC.setTime(postgresFormat.parse(rs.getString(2)));
//                    endC.add(Calendar.MONTH, 1);
//                    lastDate = postgresFormat.format(endC.getTime());
//                    //if (reportItem != null) {
//                    //fillWithZeros(postgresFormat.parse(lastDate), endDateForZeros, reportItem);
//                }
//            }
//            /*
//            jedis.put(redis_key, "persistent", "false");
//            jedis.put(redis_key, "query", st.toString());
//            jedis.put(redis_key, "result", toJson(reportItems));
//            jedis.put(redis_key, "fetchMode", "3");
//             */
//        } catch (Exception e) {
//            log.error("Batch Item Report failed: ", e);
//        } finally {
//            DbUtils.closeQuietly(rs);
//            DbUtils.closeQuietly(st);
//            DbUtils.closeQuietly(connection);
//        }
//    }

    private void fillWithZeros(Date from, Date to, COUNTER_Platform_Usage reportItem) {
        SimpleDateFormat report_dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(from);

        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(to);
        while (from.before(to)) {
            Calendar temp_c = Calendar.getInstance();
            temp_c.setTime(from);
            temp_c.set(Calendar.DAY_OF_MONTH, temp_c.getActualMaximum(Calendar.DAY_OF_MONTH));
            Date temp_endDate = temp_c.getTime();

            reportItem.addPerformance(new COUNTER_Item_Performance(report_dateFormat.format(from), report_dateFormat.format(temp_endDate), "0", "0", "0", "0"));
            fromCalendar.add(Calendar.MONTH, 1);
            from = fromCalendar.getTime();
        }
    }

    public String getInstitutionName(String repositoryIdentifier) {
        PreparedStatement st = null;
        Connection connection = null;
        ResultSet rs = null;
        String institutionName = "";
        try {
            connection = usageStatsDB.getConnection();
            st = connection.prepareStatement("select name from " + statsDB + ".datasource where id=?");
            st.setString(1, repositoryIdentifier);

            rs = st.executeQuery();
            while (rs.next()) {
                institutionName = rs.getString(1);
            }
            return institutionName;
        } catch (Exception e) {
            log.error("Repository name failed: ", e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }

        return "-1";
    }

    public boolean checkServiceConnection() {
        Connection connection = null;
        try {
            connection = usageStatsDB.getConnection();
            if (connection != null) {
                return true;
            }
        } catch (Exception e) {
            log.info(e);
        } finally {
            DbUtils.closeQuietly(connection);
        }
        return false;
    }

    public ArrayList buildMembersList() {
        PreparedStatement st = null;
        Connection connection = null;
        ResultSet rs = null;
        ArrayList membersList = new ArrayList();

        try {
            connection = usageStatsDB.getConnection();
            st = connection.prepareStatement("select distinct us.repository_id, d.name, do.oid from " + statsDB + ".datasource d, "
                    + statsDB + ".datasource_oids do, " + usagestatsImpalaDB + ".usage_stats us "
                    + "where us.repository_id=d.id and us.repository_id=do.id and d.id=do.id and do.oid not like '%piwik%' order by do.oid asc");
            rs = st.executeQuery();
            while (rs.next()) {
                ArrayList identifiers = new ArrayList();
                identifiers.add(new SUSHI_Org_Identifiers("OpenAIRE", rs.getString(1)));
                identifiers.add(new SUSHI_Org_Identifiers("OpenDOAR", rs.getString(3)));

                membersList.add(new SUSHI_Consortium_Member_List(null, null, rs.getString(2), null, identifiers));
            }

        } catch (Exception e) {
            log.error("No member found " + e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
        return membersList;

    }

    public boolean checkIfDatacite(String repositoryIdentifer) {
        PreparedStatement st = null;
        Connection connection = null;
        ResultSet rs = null;
        boolean dataciteMember = false;

        try {
            connection = usageStatsDB.getConnection();
            st = connection.prepareStatement("select distinct source from " + usagestatsImpalaDB + ".usage_stats where repository_id=?;");
            st.setString(1, repositoryIdentifer);

            rs = st.executeQuery();

            while (rs.next()) {
                if (rs.getString(1).equals("Datacite")) {
                    dataciteMember = true;
                }
            }

        } catch (Exception e) {
            log.error("No member found " + e);
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(st);
            DbUtils.closeQuietly(connection);
        }
        return dataciteMember;
    }
}
