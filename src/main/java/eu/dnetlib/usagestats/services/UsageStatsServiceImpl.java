package eu.dnetlib.usagestats.services;

import eu.dnetlib.usagestats.portal.CountryRepositories;
import eu.dnetlib.usagestats.portal.CountryUsageStats;
import eu.dnetlib.usagestats.portal.CountryUsageStatsAll;
import eu.dnetlib.usagestats.portal.MonthlyUsageStats;
import eu.dnetlib.usagestats.portal.TotalStats;
import eu.dnetlib.usagestats.portal.TotalStatsReposViewsDownloads;
import eu.dnetlib.usagestats.portal.UsageStats;
import eu.dnetlib.usagestats.repositories.UsageStatsRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

@Service
public class UsageStatsServiceImpl implements UsageStatsService {

    private final UsageStatsRepository usageStatsRepository;
    private final Logger log = Logger.getLogger(this.getClass());
    
    @Value("${prod.statsdb}")
    private String statsDB;
    @Value("${prod.usagestatsImpalaDB}")
    private String usagestatsImpalaDB;
    

    public UsageStatsServiceImpl(UsageStatsRepository usageStatsRepository) {
        this.usageStatsRepository = usageStatsRepository;
    }

    @Override
    public UsageStats getDatasourceClicks(String id) {
        String query = "SELECT 'views', sum(s.count), sum(s.openaire) FROM "+usagestatsImpalaDB+".views_stats s where s.repository_id=? "
                + "UNION ALL SELECT 'downloads', sum(s.count), sum(s.openaire) FROM "+usagestatsImpalaDB+".downloads_stats s where s.repository_id=? "
                + "UNION ALL SELECT 'pageviews', sum(s.count), 0 FROM "+usagestatsImpalaDB+".pageviews_stats s, "+statsDB+".result_datasources rd where rd.id=s.result_id and rd.datasource=? ";

        List<String> values = new ArrayList<>();
        values.add(id);
        values.add(id);
        values.add(id);

        return usageStatsRepository.executeUsageStats(query, values, "datasource");
    }

    /*
    @Override
    public UsageStats getOrganizationClicks(String organizationId) {

        String query = "select sum(number_of_views) from organization_stats where id=?";

        List<String> values = new ArrayList<>();
        values.add(organizationId);

        return usageStatsRepository.executeUsageStats(query, values, "organization");

    }
     */
    @Override
    public UsageStats getProjectClicks(String projectId) {
        String query = "SELECT 'views', sum(s.count), sum(s.openaire) FROM "+usagestatsImpalaDB+".views_stats s, "+statsDB+".project_results pr where pr.result=s.result_id and pr.id=? "
                + "UNION ALL SELECT 'downloads', sum(s.count), sum(s.openaire) FROM "+usagestatsImpalaDB+".downloads_stats s, "+statsDB+".project_results pr where pr.result=s.result_id and pr.id=? "
                + "UNION ALL SELECT 'pageviews', sum(s.count), 0 FROM "+usagestatsImpalaDB+".pageviews_stats s, "+statsDB+".project_results pr where pr.result=s.result_id and pr.id=?;";

        List<String> values = new ArrayList<>();
        values.add(projectId);
        values.add(projectId);
        values.add(projectId);

        return usageStatsRepository.executeUsageStats(query, values, "project");
    }

    @Override
    public UsageStats getResultClicks(String id) {
        String query = "SELECT 'views', s.repository_id, CASE WHEN s.source='OpenAIRE' THEN d.name ELSE concat(d.name,' - ',s.source) END, sum(count), sum(openaire) FROM (select distinct * from "+usagestatsImpalaDB+".views_stats) s, "+statsDB+".datasource d WHERE s.repository_id=d.id AND s.result_id=? GROUP BY s.source, s.repository_id, d.name "
                + "UNION ALL SELECT 'downloads', s.repository_id, CASE WHEN s.source='OpenAIRE' THEN d.name ELSE concat(d.name,' - ',s.source) END, sum(count), sum(s.openaire) FROM (select distinct * from "+usagestatsImpalaDB+".downloads_stats) s, "+statsDB+".datasource d WHERE s.repository_id=d.id AND s.result_id=? GROUP BY s.source, s.repository_id, d.name "
                + "UNION ALL SELECT 'pageviews', 'OpenAIRE id', 'OpenAIRE', sum(count), 0 FROM "+usagestatsImpalaDB+".pageviews_stats s WHERE result_id=?;";

        List<String> values = new ArrayList<>();
        values.add(id);
        values.add(id);
        values.add(id);

        return usageStatsRepository.executeUsageStats(query, values, "result");
    }

    @Override
    public UsageStats getResultClicksByPID(String resultPID) {
        String query = "SELECT 'views', s.repository_id, CASE WHEN s.source='OpenAIRE' THEN d.name ELSE concat(d.name,' - ',s.source) END, sum(count), sum(openaire) "
                + "FROM (select distinct * from "+usagestatsImpalaDB+".views_stats) s, "+statsDB+".datasource d WHERE s.repository_id=d.id "
                + "AND s.result_id in (select distinct id from "+ statsDB+".result_oids WHERE oid=?) GROUP BY s.source, s.repository_id, d.name "
                + "UNION ALL SELECT 'downloads', s.repository_id, CASE WHEN s.source='OpenAIRE' THEN d.name ELSE concat(d.name,' - ',s.source) END, sum(count), sum(s.openaire) "
                + "FROM (select distinct * from "+usagestatsImpalaDB+".downloads_stats) s, "+statsDB+".datasource d WHERE s.repository_id=d.id "
                + "AND s.result_id in (select distinct id from "+ statsDB+".result_oids WHERE oid=?)  GROUP BY s.source, s.repository_id, d.name "
                + "UNION ALL SELECT 'pageviews', 'OpenAIRE id', 'OpenAIRE', sum(count), 0 FROM "+usagestatsImpalaDB+".pageviews_stats s "
                + "WHERE result_id in (select distinct id from "+ statsDB+".result_oids WHERE oid=?);";

        List<String> values = new ArrayList<>();
        values.add(resultPID);
        values.add(resultPID);
        values.add(resultPID);

        return usageStatsRepository.executeUsageStats(query, values, "result");
    }
    @Override
    public TotalStats getTotalStats() {
        return usageStatsRepository.executeTotalStats();
    }

    @Override
    public List<MonthlyUsageStats> getMonthlyUsageStats() {
        String query = "select `date`, sum(downloads) as downloads, sum(views) as views from "+usagestatsImpalaDB+".usage_stats group by `date`getTotalStatsReposViewsDownloads order by `date` asc";
        return usageStatsRepository.executeMontlyUsageStats(query);
    }

    @Override
    public List<MonthlyUsageStats> getMonthlyUsageStatsForRepo(String id) {
        String query = "select `date`, sum(downloads) as downloads, sum(views) as views from "+usagestatsImpalaDB+".usage_stats where repository_id=? group by `date` order by `date` asc";
        return usageStatsRepository.executeMontlyUsageStatsForRepo(query,id);
    }
    
   /* @Override
    public List<CountryUsageStats> getCountryUsageStats() {
        String query = "select c.name, sum(views) as views, sum(downloads) as downloads from public.torganization t, public.organization_datasources o, public.country c, usage_stats where o.datasource=t.id\n" +
                        "and c.code=t.country and o.id=repository_id group by c.name ";
        return usageStatsRepository.executeCountryUsageStats(query);
    }*/
    @Override
    public CountryUsageStatsAll getCountryUsageStatsAll() {
        String query = "SELECT c.name, count(distinct repository_id) as total_repos, sum(views) as views, sum(downloads) as downloads "
                + "FROM "+statsDB+".organization t, "+statsDB+".organization_datasources o, "
                + ""+statsDB+".country c, "+usagestatsImpalaDB+".usage_stats "
                + "WHERE o.id=t.id AND c.code=t.country AND o.datasource=repository_id GROUP BY c.name";
        return usageStatsRepository.executeCountryUsageStats(query);
    }
    @Override
    public CountryUsageStats getCountryUsageStats(String country) {
        String query = "SELECT count(distinct repository_id) as total_repos, sum(views) as views, sum(downloads) as downloads "
                + "from "+statsDB+".organization t, "+statsDB+".organization_datasources o, "
                + ""+statsDB+".country c, "+usagestatsImpalaDB+".usage_stats where o.id=t.id and c.code=t.country and o.datasource=repository_id "
                + "and c.name='"+country+"'";
        log.info("Country Usage Stats query "+query);
        return usageStatsRepository.executeCountryUsageStats(query, country);
    }


    @Override
    public List<CountryRepositories> getCountryRepositories() {
        String query = "SELECT c.name, d.name FROM "+statsDB+".datasource d, "+statsDB+".organization t, "
                + statsDB+".organization_datasources o, "+statsDB+".country c, "+usagestatsImpalaDB+".usage_stats "
                + "WHERE o.datasource=t.id AND c.code=t.country AND o.datasource=repository_id and repository_id=d.datasource "
                + "GROUP BY d.name, c.name ORDER BY c.name";
        return usageStatsRepository.executeCountryRepositories(query);
    }

    @Override
    public TotalStatsReposViewsDownloads getTotalStatsReposViewsDownloads() {
        String query = "SELECT COUNT(DISTINCT repository_id) as repositories, sum(views) as views, sum(downloads) as downloads from "+usagestatsImpalaDB+".usage_stats";
        log.info("Total Repositories-Views-Downlaods "+query);
        return usageStatsRepository.executeTotalStatsReposViewsDownloads(query);

    }

}
