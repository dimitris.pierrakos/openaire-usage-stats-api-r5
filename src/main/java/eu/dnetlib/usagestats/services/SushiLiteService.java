package eu.dnetlib.usagestats.services;

import eu.dnetlib.usagestats.sushilite.domain.COUNTER_Dataset_Report;
import eu.dnetlib.usagestats.sushilite.domain.COUNTER_Item_Report;
import eu.dnetlib.usagestats.sushilite.domain.COUNTER_Platform_Report;
import eu.dnetlib.usagestats.sushilite.domain.SUSHI_Service_Status;
import java.util.ArrayList;
import java.util.List;

public interface SushiLiteService {

//    ReportResponseWrapper buildReport(String reportName, String release, String requestorId, String beginDate,
//            String endDate, String repositoryIdentifier, String itemIdentifier,
//            String itemDataType, String hasDoi, String granularity, String callback);
//
//    String displayReport(String reportName, String release, String requestorId, String beginDate,
//            String endDate, String repositoryIdentifier, String itemIdentifier,
//            String itemDataType, String hasDoi, String granularity, String callback, String pretty);

    SUSHI_Service_Status buildReportStatus();
    String displayReportStatus();
    
    ArrayList buildReportSupported();
    String displayReportsSupported();  
    
    ArrayList buildMembersList();
    String displayConsortiumMemberList();

    COUNTER_Platform_Report buildReportPR(String customerID, String repositoryIdentifier, String beginDate,String endDate, String metricType, String dataType,String granularity);
    String displayReportPR(String customerID, String repositoryIdentifier, String beginDate,String endDate, String metricType, String dataType,String granularity);

    COUNTER_Platform_Report buildReportPR_P1(String customerID, String repositoryIdentifier, String beginDate,String endDate);
    String displayReportPR_P1(String customerID, String repositoryIdentifier, String beginDate, String endDate);

    COUNTER_Item_Report buildReportIR(String customerID, String repositoryIdentifier, String itemIdentifier, String beginDate,String endDate, List<String> metricType, String dataType,String granularity) throws Exception;
    String displayReportIR(String customerID, String repositoryIdentifier, String itemIdentifier, String beginDate,String endDate, List<String> metricType, String dataType,String granularity);
    
    COUNTER_Dataset_Report buildReportDSR(String customerID, String repositoryIdentifier, String itemIdentifier, String beginDate,String endDate, List<String> metricType, String granularity) throws Exception;
    String displayReportDSR(String customerID, String repositoryIdentifier, String itemIdentifier, String beginDate,String endDate, List<String> metricType, String granularity);
}
