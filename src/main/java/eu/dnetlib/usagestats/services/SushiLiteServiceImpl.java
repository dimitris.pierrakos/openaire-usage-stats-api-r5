package eu.dnetlib.usagestats.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.usagestats.repositories.UsageStatsRepository;
import eu.dnetlib.usagestats.sushilite.domain.Alert;
import eu.dnetlib.usagestats.sushilite.domain.COUNTER_Dataset_Report;
import eu.dnetlib.usagestats.sushilite.domain.COUNTER_Dataset_Usage;
import eu.dnetlib.usagestats.sushilite.domain.COUNTER_Item_Report;
import eu.dnetlib.usagestats.sushilite.domain.COUNTER_Item_Usage;
import eu.dnetlib.usagestats.sushilite.domain.Filter;
import eu.dnetlib.usagestats.sushilite.domain.SUSHI_Org_Identifiers;
import eu.dnetlib.usagestats.sushilite.domain.COUNTER_Platform_Usage;
import eu.dnetlib.usagestats.sushilite.domain.SUSHI_Error_Model;
import eu.dnetlib.usagestats.sushilite.domain.COUNTER_Platform_Report;
import eu.dnetlib.usagestats.sushilite.domain.SUSHI_Service_Status;
import eu.dnetlib.usagestats.sushilite.domain.SUSHI_Report_List;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

@Service
public class SushiLiteServiceImpl implements SushiLiteService {

    private final UsageStatsRepository usageStatsRepository;

    private final Logger log = Logger.getLogger(this.getClass());

    @Value("${compression.max_number_of_records}")
    private int compression_max_number_of_records;

    @Value("${download.folder}")
    private String download_folder;

    @Value("${sushi-lite.server}")
    private String sushi_lite_server;

    boolean reportForCompression = false;

    public SushiLiteServiceImpl(UsageStatsRepository usageStatsRepository) {
        this.usageStatsRepository = usageStatsRepository;
    }

    private Date tryParse(String dateString) {
        try {
            if (dateString.length() == 7) {
                return new SimpleDateFormat("yyyy-MM").parse(dateString);
            } else if (dateString.length() == 10) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
            }
        } catch (Exception e) {
            log.error("ParseError: ", e);
        }
        return null;
    }

    @Override
    public SUSHI_Service_Status buildReportStatus() {

        ZonedDateTime dateTime = ZonedDateTime.now(); // Gets the current date and time, with your default time-zone

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Alert alert = new Alert(dateTime.format(formatter), "No alert");
        ArrayList alertsList = new ArrayList();
        alertsList.add(alert);
        try {
            if (usageStatsRepository.checkServiceConnection() == true) {

                SUSHI_Service_Status reportStatus = new SUSHI_Service_Status("COUNTER R5 Usage Reports by OpenAIRE UsageCounts Service", true, "https://provide.openaire.eu", "UsageCounts Service Info: https://usagecounts.openaire.eu", alertsList);
                return (reportStatus);
            } else {
                return null;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    @Override
    public String displayReportStatus() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            if (buildReportStatus() != null) {
//                return "<pre>" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(buildReportStatus()) + "</pre>";
                return objectMapper.writer().writeValueAsString(buildReportStatus());
            } else {
                ZonedDateTime dateTime = ZonedDateTime.now(); // Gets the current date and time, with your default time-zone
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
                SUSHI_Error_Model errorModel = new SUSHI_Error_Model("1000", "Fatal", "Service Not Available", "usagecounts.openaire.eu", "Request was for: " + dateTime.format(formatter));
//                return "<pre>" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(errorModel) + "</pre>";
                return objectMapper.writer().writeValueAsString(errorModel);

            }
        } catch (JsonProcessingException ex) {
            java.util.logging.Logger.getLogger(SushiLiteServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ArrayList buildReportSupported() {
        ArrayList reportSupportedList = new ArrayList();
        SUSHI_Report_List r1 = new SUSHI_Report_List("Status Report", "Status Report", "5", "Current status of the reporting service supported by this API", "/status");
        SUSHI_Report_List r2 = new SUSHI_Report_List("Members Report", "Members Report", "5", "List of UsageCounts members", "/members");
        SUSHI_Report_List r3 = new SUSHI_Report_List("List Of Reports Report", "", "5", "List of reports supported by the API", "/reports/");
        SUSHI_Report_List r4 = new SUSHI_Report_List("Platform Master Report PR", "PR", "5", "A customizable report summarizing activity across a provider’s platforms that allows the user to apply filters and select other configuration options for the report. ", "/PR");
        SUSHI_Report_List r5 = new SUSHI_Report_List("Platform Usage Report", "PR_P1", "5", "Standard View of the Package Master Report that presents usage for the overall Platform broken down by Metric_Type.", "/PR_1");
        SUSHI_Report_List r6 = new SUSHI_Report_List("Platform Item Report", "IR", "5", "COUNTER Item Master Report", "/IR");
        SUSHI_Report_List r7 = new SUSHI_Report_List("Datasets Report", "DSR", "5", "COUNTER Datasets Report", "/DSR");

        reportSupportedList.add(r1);
        reportSupportedList.add(r2);
        reportSupportedList.add(r3);
        reportSupportedList.add(r4);
        reportSupportedList.add(r5);
        reportSupportedList.add(r6);
        reportSupportedList.add(r7);

        return reportSupportedList;
    }

    @Override
    public String displayReportsSupported() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
//            return "<pre>" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(buildReportSupported()) + "</pre>";
            return objectMapper.writer().writeValueAsString(buildReportSupported());
        } catch (JsonProcessingException ex) {
            java.util.logging.Logger.getLogger(SushiLiteServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public COUNTER_Platform_Report buildReportPR(String customerID, String repoID, String beginDate,
            String endDate, String metricType, String dataType, String granularity) {
        List<SUSHI_Error_Model> reportExceptions = new ArrayList<>();
        ZonedDateTime dateTime = ZonedDateTime.now(); // Gets the current date and time, with your default time-zone
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        //Display Report Created Day

        String repositoryIdentifier = usageStatsRepository.getInstitutionID(repoID);
        if (!repositoryIdentifier.equals("")) {
            if (repositoryIdentifier.equals("-1")) {
                reportExceptions.add(new SUSHI_Error_Model("3060", "Error", "Invalid Filter Value", "usagecounts.openaire.eu", "RepositoryIdentifier: " + repoID + " is not valid"));
            }
        }

        Date beginDateParsed;
        if (!beginDate.equals("")) {
            beginDateParsed = tryParse(beginDate);
            if (beginDateParsed != null && (granularity.toLowerCase().equals("monthly") || beginDate.length() == 7)) {
                Calendar temp = Calendar.getInstance();
                temp.setTime(beginDateParsed);
                temp.set(Calendar.DAY_OF_MONTH, temp.getActualMinimum(Calendar.DAY_OF_MONTH));
                beginDateParsed = temp.getTime();
            } else if (beginDateParsed == null) {
                reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "usagecounts.openaire.eu", "Invalid Date Arguments", "Begin Date: " + beginDate + " is not a valid date"));
            }
        } else {
            Calendar temp = Calendar.getInstance();
            temp.add(Calendar.MONTH, -1);
            temp.set(Calendar.DAY_OF_MONTH, temp.getActualMinimum(Calendar.DAY_OF_MONTH));
            beginDateParsed = temp.getTime();
            reportExceptions.add(new SUSHI_Error_Model("3021", "Warning", "usagecounts.openaire.eu", "Unspecified Date Arguments", "Begin Date set to default: " + new SimpleDateFormat("yyyy-MM-dd").format(beginDateParsed)));
        }

        Date endDateParsed;
        if (!endDate.equals("")) {
            endDateParsed = tryParse(endDate);
            if (endDateParsed != null && (granularity.toLowerCase().equals("monthly") || endDate.length() == 7)) {
                Calendar temp = Calendar.getInstance();
                temp.setTime(endDateParsed);
                temp.set(Calendar.DAY_OF_MONTH, temp.getActualMaximum(Calendar.DAY_OF_MONTH));
                endDateParsed = temp.getTime();
            } else if (endDateParsed == null) {
                reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "usagecounts.openaire.eu", "Invalid Date Arguments", "End Date: " + endDate + " is not a valid date"));
            }
        } else {
            Calendar temp = Calendar.getInstance();
            temp.add(Calendar.MONTH, -1);
            temp.set(Calendar.DAY_OF_MONTH, temp.getActualMaximum(Calendar.DAY_OF_MONTH));
            endDateParsed = temp.getTime();
            reportExceptions.add(new SUSHI_Error_Model("3021", "Warning", "usagecounts.openaire.eu", "Unspecified Date Arguments", "End Date set to default: " + new SimpleDateFormat("yyyy-MM-dd").format(endDateParsed)));
        }
        //log.error("dates: " + beginDateParsed.toString() + " - " + endDateParsed.toString());

        if (beginDateParsed == null) {
            reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "usagecounts.openaire.eu", "Invalid Date Arguments", "Begin Date: " + beginDate + " is not a valid date"));
        }
        if (endDateParsed == null) {
            reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "usagecounts.openaire.eu", "Invalid Date Arguments", "End Date: " + endDate + " is not a valid date"));
        }
        if (beginDateParsed != null && endDateParsed != null && !beginDateParsed.before(endDateParsed)) {
            reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "usagecounts.openaire.eu", "Invalid Date Arguments", "BeginDate \'" + new SimpleDateFormat("yyyy-MM-dd").format(beginDateParsed) + "\' is greater than EndDate \'" + new SimpleDateFormat("yyyy-MM-dd").format(endDateParsed) + "\'"));
        }

        List<Filter> reportFilters = new ArrayList();
        reportFilters.add(new Filter("BeginDate", beginDate));
        reportFilters.add(new Filter("EndDate", endDate));
        String reportID = "PR";
        String reportName = "Platform Master Report";
        //String institutionName = "Insititution Name " + repositoryIdentifier;
        String institutionName = usageStatsRepository.getInstitutionName(repositoryIdentifier);
        List<SUSHI_Org_Identifiers> institutionIdD = new ArrayList();
        institutionIdD.add(new SUSHI_Org_Identifiers("Openaire", repositoryIdentifier));

        List<COUNTER_Platform_Usage> reportItems = new ArrayList();
        if (reportExceptions.size() == 0) {
            reportExceptions = null;
            usageStatsRepository.executeBatchItemsPR(reportItems, repositoryIdentifier, beginDateParsed, endDateParsed, metricType, dataType, granularity);
        }

        if (reportItems.isEmpty()) {
            reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
        }

        COUNTER_Platform_Report reportPr = new COUNTER_Platform_Report(dateTime.format(formatter), customerID, reportID, reportName, institutionName, institutionIdD, reportExceptions, reportFilters, reportItems);

        log.info("Total report items " + reportItems.size());

        if (reportItems.size() > compression_max_number_of_records) {
            log.info("Compression due to " + reportItems.size());
            reportForCompression = true;
        }
        return reportPr;
    }

    @Override
    public COUNTER_Platform_Report buildReportPR_P1(String customerID, String repoID,
            String beginDate,
            String endDate) {
        String granularity = "monthly";

        List<SUSHI_Error_Model> reportExceptions = new ArrayList<>();
        ZonedDateTime dateTime = ZonedDateTime.now(); // Gets the current date and time, with your default time-zone
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        //Display Report Created Day

        String repositoryIdentifier = usageStatsRepository.getInstitutionID(repoID);
        if (!repositoryIdentifier.equals("")) {
            if (repositoryIdentifier.equals("-1")) {
                reportExceptions.add(new SUSHI_Error_Model("3060", "Error", "Invalid Filter Value", "usagecounts.openaire.eu", "RepositoryIdentifier: " + repoID + " is not valid"));
            }
        }
        Date beginDateParsed;
        if (!beginDate.equals("")) {
            beginDateParsed = tryParse(beginDate);
            if (beginDateParsed != null && (granularity.toLowerCase().equals("monthly") || beginDate.length() == 7)) {
                Calendar temp = Calendar.getInstance();
                temp.setTime(beginDateParsed);
                temp.set(Calendar.DAY_OF_MONTH, temp.getActualMinimum(Calendar.DAY_OF_MONTH));
                beginDateParsed = temp.getTime();
            }
        } else {
            Calendar temp = Calendar.getInstance();
            temp.add(Calendar.MONTH, -1);
            temp.set(Calendar.DAY_OF_MONTH, temp.getActualMinimum(Calendar.DAY_OF_MONTH));
            beginDateParsed = temp.getTime();
            reportExceptions.add(new SUSHI_Error_Model("3021", "Warning", "usagecounts.openaire.eu", "Unspecified Date Arguments", "Begin Date set to default: " + new SimpleDateFormat("yyyy-MM-dd").format(beginDateParsed)));
        }

        Date endDateParsed;
        if (!endDate.equals("")) {
            endDateParsed = tryParse(endDate);
            if (endDateParsed != null && (granularity.toLowerCase().equals("monthly") || endDate.length() == 7)) {
                Calendar temp = Calendar.getInstance();
                temp.setTime(endDateParsed);
                temp.set(Calendar.DAY_OF_MONTH, temp.getActualMaximum(Calendar.DAY_OF_MONTH));
                endDateParsed = temp.getTime();
            }
        } else {
            Calendar temp = Calendar.getInstance();
            temp.add(Calendar.MONTH, -1);
            temp.set(Calendar.DAY_OF_MONTH, temp.getActualMaximum(Calendar.DAY_OF_MONTH));
            endDateParsed = temp.getTime();
            reportExceptions.add(new SUSHI_Error_Model("3021", "Warning", "usagecounts.openaire.eu", "Unspecified Date Arguments", "End Date set to default: " + new SimpleDateFormat("yyyy-MM-dd").format(endDateParsed)));
        }
        //log.error("dates: " + beginDateParsed.toString() + " - " + endDateParsed.toString());

        if (beginDateParsed == null) {
            reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "usagecounts.openaire.eu", "Invalid Date Arguments", "Begin Date: " + beginDate + " is not a valid date"));
        }
        if (endDateParsed == null) {
            reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "usagecounts.openaire.eu", "Invalid Date Arguments", "End Date: " + endDate + " is not a valid date"));
        }
        if (beginDateParsed != null && endDateParsed != null && !beginDateParsed.before(endDateParsed)) {
            reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "usagecounts.openaire.eu", "Invalid Date Arguments", "BeginDate \'" + new SimpleDateFormat("yyyy-MM-dd").format(beginDateParsed) + "\' is greater than EndDate \'" + new SimpleDateFormat("yyyy-MM-dd").format(endDateParsed) + "\'"));
        }

        List<Filter> reportFilters = new ArrayList();
        reportFilters.add(new Filter("BeginDate", beginDate));
        reportFilters.add(new Filter("EndDate", endDate));
        String reportID = "PR_P1";
        String reportName = "Platform Master Report";
        //String institutionName = "Insititution Name " + repositoryIdentifier;
        String institutionName = usageStatsRepository.getInstitutionName(repositoryIdentifier);
        List<SUSHI_Org_Identifiers> institutionIdD = new ArrayList();
        institutionIdD.add(new SUSHI_Org_Identifiers("Openaire", repositoryIdentifier));

        List<COUNTER_Platform_Usage> reportItems = new ArrayList();
        usageStatsRepository.executeBatchItemsPR_P1(reportItems, repositoryIdentifier, beginDateParsed, endDateParsed);
        if (reportItems.isEmpty()) {
            reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
        }

        if (reportExceptions.size() == 0) {
            reportExceptions = null;
        }

        COUNTER_Platform_Report reportPr = new COUNTER_Platform_Report(dateTime.format(formatter), customerID, reportID, reportName, institutionName, institutionIdD, reportExceptions, reportFilters, reportItems);
        return reportPr;
    }

    @Override
    public COUNTER_Item_Report buildReportIR(String customerID, String repoID, String itemIdentifier, String beginDate,
            String endDate, List<String> metricType, String dataType, String granularity) throws Exception {

        ZonedDateTime dateTime = ZonedDateTime.now(); // Gets the current date and time, with your default time-zone
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

        List<COUNTER_Item_Usage> reportItems = new ArrayList<>();
        List<SUSHI_Error_Model> reportExceptions = new ArrayList<>();
        List<Filter> reportFilters = new ArrayList();
        List<SUSHI_Org_Identifiers> orgIdentifiers = new ArrayList<>();
        reportFilters.add(new Filter("BeginDate", beginDate));
        reportFilters.add(new Filter("EndDate", endDate));

        String repositoryIdentifier = "";

        if (!repoID.equals("")) {
            repositoryIdentifier = usageStatsRepository.getInstitutionID(repoID);
            if (repositoryIdentifier.equals("-1")) {
                reportExceptions.add(new SUSHI_Error_Model("3060", "Error", "Invalid Filter Value", "usagecounts.openaire.eu", "RepositoryIdentifier: " + repoID + " is not valid"));
            }
        }

        if(repositoryIdentifier.equals(""))
            orgIdentifiers=null;
        else
            orgIdentifiers.add(new SUSHI_Org_Identifiers("Openaire", repositoryIdentifier));

        if (!granularity.equalsIgnoreCase("totals") && !granularity.equalsIgnoreCase("monthly")) {
            reportExceptions.add(new SUSHI_Error_Model("3062", "Warning", "Invalid ReportAttribute Value", "usagecounts.openaire.eu", "Granularity: \'" + granularity + "\' unknown. Defaulting to Monthly"));
            granularity = "Monthly";
        }

        Date beginDateParsed;
        if (!beginDate.equals("")) {
            beginDateParsed = tryParse(beginDate);
            if (beginDateParsed != null && (granularity.toLowerCase().equals("monthly") || beginDate.length() == 7)) {
                Calendar temp = Calendar.getInstance();
                temp.setTime(beginDateParsed);
                temp.set(Calendar.DAY_OF_MONTH, temp.getActualMinimum(Calendar.DAY_OF_MONTH));
                beginDateParsed = temp.getTime();
            } else if (beginDateParsed == null) {
                reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "Invalid Date Arguments", "usagecounts.openaire.eu", "Begin Date: " + beginDate + " is not a valid date"));
            }
        } else {
            Calendar temp = Calendar.getInstance();
            temp.add(Calendar.MONTH, -1);
            temp.set(Calendar.DAY_OF_MONTH, temp.getActualMinimum(Calendar.DAY_OF_MONTH));
            beginDateParsed = temp.getTime();
            reportExceptions.add(new SUSHI_Error_Model("3021", "Warning", "Unspecified Date Arguments", "usagecounts.openaire.eu", "Begin Date set to default: " + new SimpleDateFormat("yyyy-MM-dd").format(beginDateParsed)));
        }

        Date endDateParsed;
        if (!endDate.equals("")) {
            endDateParsed = tryParse(endDate);
            if (endDateParsed != null && (granularity.toLowerCase().equals("monthly") || endDate.length() == 7)) {
                Calendar temp = Calendar.getInstance();
                temp.setTime(endDateParsed);
                temp.set(Calendar.DAY_OF_MONTH, temp.getActualMaximum(Calendar.DAY_OF_MONTH));
                endDateParsed = temp.getTime();
            } else if (endDateParsed == null) {
                reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "Invalid Date Arguments", "usagecounts.openaire.eu", "End Date: " + endDate + " is not a valid date"));
            }
        } else {
            Calendar temp = Calendar.getInstance();
            temp.add(Calendar.MONTH, -1);
            temp.set(Calendar.DAY_OF_MONTH, temp.getActualMaximum(Calendar.DAY_OF_MONTH));
            endDateParsed = temp.getTime();
            reportExceptions.add(new SUSHI_Error_Model("3021", "Warning", "Unspecified Date Arguments", "usagecounts.openaire.eu", "End Date set to default: " + new SimpleDateFormat("yyyy-MM-dd").format(endDateParsed)));
        }

  
            // Calucalte time difference
            // in milliseconds
//            beginDateParsed = tryParse(beginDate);
//            Calendar temp = Calendar.getInstance();
//            temp.setTime(beginDateParsed);
//            temp.set(Calendar.DAY_OF_MONTH, temp.getActualMaximum(Calendar.DAY_OF_MONTH));
//            beginDateParsed = temp.getTime();
//
//
//            endDateParsed = tryParse(endDate);
//            Calendar temp1 = Calendar.getInstance();
//            temp1.setTime(endDateParsed);
//            temp1.set(Calendar.DAY_OF_MONTH, temp1.getActualMaximum(Calendar.DAY_OF_MONTH));
//            endDateParsed = temp1.getTime();

            long difference_In_Time
                = endDateParsed.getTime() - beginDateParsed.getTime();
            long difference_In_Years = (difference_In_Time/ (1000 * 60 * 60 * 24));

            if(difference_In_Years>365)
                reportExceptions.add(new SUSHI_Error_Model("4000", "Notice", "Requested Period for more than a year not allowed", "usagecounts.openaire.eu", "Contact usagecounts@openaire.eu for longer period"));

        if (beginDateParsed != null && endDateParsed != null && !beginDateParsed.before(endDateParsed)) {
            reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "Invalid Date Arguments", "usagecounts.openaire.eu", "BeginDate \'" + new SimpleDateFormat("yyyy-MM-dd").format(beginDateParsed) + "\' is greater than EndDate \'" + new SimpleDateFormat("yyyy-MM-dd").format(endDateParsed) + "\'"));
        }

        String institutionName=null;
        if(repositoryIdentifier!="-1")
            institutionName = usageStatsRepository.getInstitutionName(repositoryIdentifier);
        if(repoID.equals(""))
            institutionName=null;

        String itemid = "";
        if (!itemIdentifier.equals("")) {
            String[] split = itemIdentifier.split(":");
            switch (split[0].toLowerCase()) {
                case "oid":
                    itemid = itemIdentifier;
                    break;
                case "doi":
                    itemid = itemIdentifier;
                    break;
//                case "openaire":
//                    itemid = itemIdentifier;
//                    break;
                default:
                    reportExceptions.add(new SUSHI_Error_Model("3060", "Error", "Invalid Filter Value", "usagecounts.openaire.eu", "ItemIdentifier: " + itemIdentifier + " is not valid"));
                    itemid = "-1";
            }
        }
        if (itemid.equals("") && repoID.equals("")) {
            reportExceptions.add(new SUSHI_Error_Model("3070", "Error", "Required Filter Missing", "usagecounts.openaire.eu", "ItemIdentifier or RepositoryIdentifier must be supplied"));
        }
        //if (reportName.equalsIgnoreCase("ar1")) {
        if (!itemid.equals("-1") && !repositoryIdentifier.equals("-1") && beginDateParsed != null && endDateParsed != null && beginDateParsed.before(endDateParsed)) {
            if (!itemid.equals("") && !repositoryIdentifier.equals("")) {
                //if (dataType.equalsIgnoreCase("") || dataType.equalsIgnoreCase("article")) {
                if (reportExceptions.size() == 0) {
                    //reportExceptions = null;
                    usageStatsRepository.executeItemIR(reportItems, repositoryIdentifier, itemIdentifier, beginDateParsed, endDateParsed, metricType, dataType, granularity);
                }

                if (reportItems.isEmpty()) {
                    reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
                }
                // } else {
                //reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
                //}
            } else if (!repoID.equals("")) {
                if (reportExceptions.size() == 0) {
                    //reportExceptions = null;
                    usageStatsRepository.executeBatchItemsIR(reportItems, repositoryIdentifier, itemIdentifier, beginDateParsed, endDateParsed, metricType, dataType, granularity);
                }
                if (reportItems.isEmpty()) {
                    reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
                }
            }
        }
        if (repoID.equals("")) {
            if (reportExceptions.isEmpty()) {
                //reportExceptions = null;
                usageStatsRepository.executeItemIR(reportItems, null, itemIdentifier, beginDateParsed, endDateParsed, metricType, dataType, granularity);
            }
            if (reportItems.isEmpty()) {
                reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
            }
        }

        if (reportItems.isEmpty()) {
            reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
        }
        if (reportExceptions.isEmpty()) {
            reportExceptions = null;
        }

        COUNTER_Item_Report reportResponse = new COUNTER_Item_Report(dateTime.format(formatter), customerID, "IR", "Item Report", institutionName, orgIdentifiers,
                reportExceptions, reportFilters, reportItems);
        log.info("Total report items " + reportItems.size());

        if (reportItems.size() > compression_max_number_of_records) {
            log.info("Compression due to " + reportItems.size());
            reportForCompression = true;
        }
        return reportResponse;
    }

    @Override
    public COUNTER_Dataset_Report buildReportDSR(String customerID, String repositoryIdentifier, String itemIdentifier, String beginDate,
            String endDate, List<String> metricType,String granularity) throws Exception {

        ZonedDateTime dateTime = ZonedDateTime.now(); // Gets the current date and time, with your default time-zone
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

        List<COUNTER_Dataset_Usage> reportItems = new ArrayList<>();
        List<SUSHI_Error_Model> reportExceptions = new ArrayList<>();
        List<Filter> reportFilters = new ArrayList();
        List<SUSHI_Org_Identifiers> orgIdentifiers = new ArrayList<>();
        reportFilters.add(new Filter("BeginDate", beginDate));
        reportFilters.add(new Filter("EndDate", endDate));

        if (!repositoryIdentifier.equals("")) {
            repositoryIdentifier = usageStatsRepository.getInstitutionID(repositoryIdentifier);
        }

        orgIdentifiers.add(new SUSHI_Org_Identifiers("Openaire", repositoryIdentifier));

        if (!granularity.equalsIgnoreCase("totals") && !granularity.equalsIgnoreCase("monthly")) {
            reportExceptions.add(new SUSHI_Error_Model("3062", "Warning", "Invalid ReportAttribute Value", "usagecounts.openaire.eu", "Granularity: \'" + granularity + "\' unknown. Defaulting to Monthly"));
            granularity = "Monthly";
        }

        Date beginDateParsed;
        if (!beginDate.equals("")) {
            beginDateParsed = tryParse(beginDate);
            if (beginDateParsed != null && (granularity.toLowerCase().equals("monthly") || beginDate.length() == 7)) {
                Calendar temp = Calendar.getInstance();
                temp.setTime(beginDateParsed);
                temp.set(Calendar.DAY_OF_MONTH, temp.getActualMinimum(Calendar.DAY_OF_MONTH));
                beginDateParsed = temp.getTime();
            }
        } else {
            Calendar temp = Calendar.getInstance();
            temp.add(Calendar.MONTH, -1);
            temp.set(Calendar.DAY_OF_MONTH, temp.getActualMinimum(Calendar.DAY_OF_MONTH));
            beginDateParsed = temp.getTime();
            reportExceptions.add(new SUSHI_Error_Model("3021", "Warning", "Unspecified Date Arguments", "usagecounts.openaire.eu", "Begin Date set to default: " + new SimpleDateFormat("yyyy-MM-dd").format(beginDateParsed)));
        }

        Date endDateParsed;
        if (!endDate.equals("")) {
            endDateParsed = tryParse(endDate);
            if (endDateParsed != null && (granularity.toLowerCase().equals("monthly") || endDate.length() == 7)) {
                Calendar temp = Calendar.getInstance();
                temp.setTime(endDateParsed);
                temp.set(Calendar.DAY_OF_MONTH, temp.getActualMaximum(Calendar.DAY_OF_MONTH));
                endDateParsed = temp.getTime();
            }
        } else {
            Calendar temp = Calendar.getInstance();
            temp.add(Calendar.MONTH, -1);
            temp.set(Calendar.DAY_OF_MONTH, temp.getActualMaximum(Calendar.DAY_OF_MONTH));
            endDateParsed = temp.getTime();
            reportExceptions.add(new SUSHI_Error_Model("3021", "Warning", "Unspecified Date Arguments", "usagecounts.openaire.eu", "End Date set to default: " + new SimpleDateFormat("yyyy-MM-dd").format(endDateParsed)));
        }

        if (beginDateParsed == null) {
            reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "Invalid Date Arguments", "usagecounts.openaire.eu", "Begin Date: " + beginDate + " is not a valid date"));
        }
        if (endDateParsed == null) {
            reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "Invalid Date Arguments", "usagecounts.openaire.eu", "End Date: " + endDate + " is not a valid date"));
        }
        if (beginDateParsed != null && endDateParsed != null && !beginDateParsed.before(endDateParsed)) {
            reportExceptions.add(new SUSHI_Error_Model("3020", "Error", "Invalid Date Arguments", "usagecounts.openaire.eu", "BeginDate \'" + new SimpleDateFormat("yyyy-MM-dd").format(beginDateParsed) + "\' is greater than EndDate \'" + new SimpleDateFormat("yyyy-MM-dd").format(endDateParsed) + "\'"));
        }

        String repoid = repositoryIdentifier;
        String institutionName = usageStatsRepository.getInstitutionName(repositoryIdentifier);

        //if (!repositoryIdentifier.equals("")) {
        //  repoid = usageStatsRepository.getInstitutionID(repositoryIdentifier);
        if (repoid.equals("-1")) {
            reportExceptions.add(new SUSHI_Error_Model("3060", "Error", "Invalid Filter Value", "usagecounts.openaire.eu", "RepositoryIdentifier: " + repositoryIdentifier + " is not valid"));
        }
        //}
        String itemid = "";
        if (!itemIdentifier.equals("")) {
            String[] split = itemIdentifier.split(":");
            switch (split[0].toLowerCase()) {
                case "oid":
                    itemid = itemIdentifier;
                    break;
                case "doi":
                    itemid = itemIdentifier;
                    break;
//                case "openaire":
//                    itemid = itemIdentifier;
//                    break;
                default:
                    reportExceptions.add(new SUSHI_Error_Model("3060", "Error", "Invalid Filter Value", "usagecounts.openaire.eu", "ItemIdentifier: " + itemIdentifier + " is not valid"));
                    itemid = "-1";
            }
        }
        if (itemid.equals("") && repoid.equals("")) {
            reportExceptions.add(new SUSHI_Error_Model("3070", "Error", "Required Filter Missing", "usagecounts.openaire.eu", "ItemIdentifier or RepositoryIdentifier must be supplied"));
        }
        //if (reportName.equalsIgnoreCase("ar1")) {
        if (!itemid.equals("-1") && !repoid.equals("-1") && beginDateParsed != null && endDateParsed != null && beginDateParsed.before(endDateParsed)) {
            if (!itemid.equals("") && !repoid.equals("")) {
                //if (dataType.equalsIgnoreCase("") || dataType.equalsIgnoreCase("article")) {
                usageStatsRepository.executeItemDSR(reportItems, repoid, itemIdentifier, beginDateParsed, endDateParsed, metricType, granularity);
                if (reportItems.isEmpty()) {
                    reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
                }
                // } else {
                //reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
                //}
            } else if (!repoid.equals("")) {
                if(usageStatsRepository.checkIfDatacite(repoid))
                    usageStatsRepository.executeBatchItemsDSRDatacite(reportItems, repoid, itemIdentifier, beginDateParsed, endDateParsed, metricType, granularity);
                else
                    usageStatsRepository.executeBatchItemsDSR(reportItems, repoid, itemIdentifier, beginDateParsed, endDateParsed, metricType, granularity);
                if (reportItems.isEmpty()) {
                    reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
                }
            }
        }
        if (repoid.equals("")) {
            usageStatsRepository.executeItemDSR(reportItems, null, itemIdentifier, beginDateParsed, endDateParsed, metricType, granularity);
            if (reportItems.isEmpty()) {
                reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
            }
        }

        if (reportItems.isEmpty()) {
            reportExceptions.add(new SUSHI_Error_Model("3030", "Error", "usagecounts.openaire.eu", "No Usage Available for Requested Dates", "Service did not find any data"));
        }

        if (reportExceptions.size() == 0) {
            reportExceptions = null;
        }
        COUNTER_Dataset_Report reportResponse = new COUNTER_Dataset_Report(dateTime.format(formatter), customerID, "DSR", "Dataset Report", institutionName, orgIdentifiers,
                reportExceptions, reportFilters, reportItems);

        return reportResponse;
    }

    @Override
    public String displayReportPR(String customerID, String repositoryIdentifier, String beginDate,
            String endDate, String metricType, String dataType, String granularity) {
        ObjectMapper objectMapper = new ObjectMapper();
        COUNTER_Platform_Report report = buildReportPR(customerID, repositoryIdentifier, beginDate, endDate, metricType, dataType, granularity);

        try {
            if (reportForCompression == false) {
//                return "<pre>" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(report) + "</pre>";
                return objectMapper.writer().writeValueAsString(report) ;
            } else {
                log.info((beginDate + " " + endDate));
                try {
                    java.sql.Timestamp timestamp1 = new java.sql.Timestamp(System.currentTimeMillis());
                    //System.out.println("String start " + timestamp1);
                    log.info("Start building report..." + timestamp1);
                    //String outputname = "PR" + "_" + repositoryIdentifier.replace("_", "").replace(":", "") +"_"+ beginDate.replace("-", "") + "_" + endDate.replace("-", "");
                    String outputname = "PR" + "_" + repositoryIdentifier.substring(repositoryIdentifier.indexOf(":") + 1) + "_" + beginDate.replace("-", "") + "_" + endDate.replace("-", "");
                    String directory = new File(download_folder).getAbsolutePath();
                    //writer.writeValue(new File(directory + "/" + outputname + ".json"), tmpReport);
                    BufferedWriter writer = new BufferedWriter(new FileWriter(directory + "/" + outputname + ".json"));
                    writer.write(objectMapper.writer().writeValueAsString(report));
                    writer.close();

                    FileOutputStream fos = new FileOutputStream(directory + "/" + outputname + ".zip");
                    ZipOutputStream zipOut = new ZipOutputStream(fos);
                    File fileToZip = new File(directory + "/" + outputname + ".json");
                    FileInputStream fis = new FileInputStream(fileToZip);
                    ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
                    zipOut.putNextEntry(zipEntry);
                    byte[] bytes = new byte[1024];
                    int length;
                    while ((length = fis.read(bytes)) >= 0) {
                        zipOut.write(bytes, 0, length);
                    }
                    zipOut.close();
                    fis.close();
                    fos.close();
                    fileToZip.delete();

                    java.sql.Timestamp timestamp2 = new java.sql.Timestamp(System.currentTimeMillis());
                    //System.out.println("String end " + timestamp2);
                    log.info("Report created..." + timestamp2);

                    reportForCompression = false;
                    return new String(sushi_lite_server + "/download/" + outputname + ".zip");
                } catch (JsonProcessingException ex) {
                    java.util.logging.Logger.getLogger(SushiLiteServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SushiLiteServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public String displayReportPR_P1(String customerID, String repositoryIdentifier,
            String beginDate,
            String endDate
    ) {
        ObjectMapper objectMapper = new ObjectMapper();
        log.info((beginDate + " " + endDate));
        try {
//            return "<pre>" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(buildReportPR_P1(customerID, repositoryIdentifier, beginDate, endDate)) + "</pre>";
            return objectMapper.writer().writeValueAsString(buildReportPR_P1(customerID, repositoryIdentifier, beginDate, endDate));
        } catch (JsonProcessingException ex) {
            java.util.logging.Logger.getLogger(SushiLiteServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public String displayReportIR(String customerID, String repositoryIdentifier,
            String itemIdentifier, String beginDate,
            String endDate, List<String> metricType,
            String dataType, String granularity
    ) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            COUNTER_Item_Report report = buildReportIR(customerID, repositoryIdentifier, itemIdentifier, beginDate, endDate, metricType, dataType, granularity);
            if (reportForCompression == false) {
//                return "<pre>" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(report) + "</pre>";
                return objectMapper.writer().writeValueAsString(report);
            } else {
                log.info((beginDate + " " + endDate));

                try {
                    java.sql.Timestamp timestamp1 = new java.sql.Timestamp(System.currentTimeMillis());
                    //System.out.println("String start " + timestamp1);
                    log.info("Start building report..." + timestamp1);
                    String outputname = "IR" + "_" + repositoryIdentifier.substring(repositoryIdentifier.indexOf(":") + 1) + "_" + beginDate.replace("-", "") + "_" + endDate.replace("-", "");
                    String directory = new File(download_folder).getAbsolutePath();
                    BufferedWriter writer = new BufferedWriter(new FileWriter(directory + "/" + outputname + ".json"));
                    writer.write(objectMapper.writer().writeValueAsString(report));
                    writer.close();

                    FileOutputStream fos = new FileOutputStream(directory + "/" + outputname + ".zip");
                    ZipOutputStream zipOut = new ZipOutputStream(fos);
                    File fileToZip = new File(directory + "/" + outputname + ".json");
                    FileInputStream fis = new FileInputStream(fileToZip);
                    ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
                    zipOut.putNextEntry(zipEntry);
                    byte[] bytes = new byte[1024];
                    int length;
                    while ((length = fis.read(bytes)) >= 0) {
                        zipOut.write(bytes, 0, length);
                    }
                    zipOut.close();
                    fis.close();
                    fos.close();
                    fileToZip.delete();

                    java.sql.Timestamp timestamp2 = new java.sql.Timestamp(System.currentTimeMillis());
                    //System.out.println("String end " + timestamp2);
                    log.info("Report created..." + timestamp2);
                    reportForCompression = false;
                    return new String(sushi_lite_server + "/download/" + outputname + ".zip");

                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(SushiLiteServiceImpl.class
                            .getName()).log(Level.SEVERE, null, ex);

                }

            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SushiLiteServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String displayReportDSR(String customerID, String repositoryIdentifier,
            String itemIdentifier, String beginDate,
            String endDate, List<String> metricType,String granularity) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            COUNTER_Dataset_Report report = buildReportDSR(customerID, repositoryIdentifier, itemIdentifier, beginDate, endDate, metricType, granularity);
            if (reportForCompression == false) {
//                return "<pre>" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(report) + "</pre>";
                return objectMapper.writer().writeValueAsString(report);
            } else {
                log.info((beginDate + " " + endDate));

                try {
                    java.sql.Timestamp timestamp1 = new java.sql.Timestamp(System.currentTimeMillis());
                    //System.out.println("String start " + timestamp1);
                    log.info("Start building report..." + timestamp1);
                    String outputname = "DSR" + "_" + repositoryIdentifier.substring(repositoryIdentifier.indexOf(":") + 1) + "_" + beginDate.replace("-", "") + "_" + endDate.replace("-", "");
                    String directory = new File(download_folder).getAbsolutePath();
                    BufferedWriter writer = new BufferedWriter(new FileWriter(directory + "/" + outputname + ".json"));
                    writer.write(objectMapper.writer().writeValueAsString(report));
                    writer.close();

                    FileOutputStream fos = new FileOutputStream(directory + "/" + outputname + ".zip");
                    ZipOutputStream zipOut = new ZipOutputStream(fos);
                    File fileToZip = new File(directory + "/" + outputname + ".json");
                    FileInputStream fis = new FileInputStream(fileToZip);
                    ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
                    zipOut.putNextEntry(zipEntry);
                    byte[] bytes = new byte[1024];
                    int length;
                    while ((length = fis.read(bytes)) >= 0) {
                        zipOut.write(bytes, 0, length);
                    }
                    zipOut.close();
                    fis.close();
                    fos.close();
                    fileToZip.delete();

                    java.sql.Timestamp timestamp2 = new java.sql.Timestamp(System.currentTimeMillis());
                    //System.out.println("String end " + timestamp2);
                    log.info("Report created..." + timestamp2);
                    reportForCompression = false;
                    return new String(sushi_lite_server + "/download/" + outputname + ".zip");

                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(SushiLiteServiceImpl.class
                            .getName()).log(Level.SEVERE, null, ex);

                }

            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SushiLiteServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    @Override
    public ArrayList buildMembersList() {
        ArrayList consortiumMembers = usageStatsRepository.buildMembersList();
        return consortiumMembers;
    }

    @Override
    public String displayConsortiumMemberList() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
//            return "<pre>" + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(buildMembersList()) + "</pre>";
            return objectMapper.writer().writeValueAsString(buildMembersList());
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SushiLiteServiceImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
}

class JSONUtil {

    public static String escape(String input) {
        StringBuilder output = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            int chx = (int) ch;

            // let's not put any nulls in our strings
            assert (chx != 0);

            if (ch == '\n') {
                output.append("\\n");
            } else if (ch == '\t') {
                output.append("\\t");
            } else if (ch == '\r') {
                output.append("\\r");
            } else if (ch == '\\') {
                output.append("\\\\");
            } else if (ch == '"') {
                output.append("\\\"");
            } else if (ch == '\b') {
                output.append("\\b");
            } else if (ch == '\f') {
                output.append("\\f");
            } else if (chx >= 0x10000) {
                assert false : "Java stores as u16, so it should never give us a character that's bigger than 2 bytes. It literally can't.";
            } else if (chx > 127) {
                output.append(String.format("\\u%04x", chx));
            } else {
                output.append(ch);
            }
        }

        return output.toString();
    }

    public static String unescape(String input) {
        StringBuilder builder = new StringBuilder();

        int i = 0;
        while (i < input.length()) {
            char delimiter = input.charAt(i);
            i++; // consume letter or backslash

            if (delimiter == '\\' && i < input.length()) {

                // consume first after backslash
                char ch = input.charAt(i);
                i++;

                if (ch == '\\' || ch == '/' || ch == '"' || ch == '\'') {
                    builder.append(ch);
                } else if (ch == 'n') {
                    builder.append('\n');
                } else if (ch == 'r') {
                    builder.append('\r');
                } else if (ch == 't') {
                    builder.append('\t');
                } else if (ch == 'b') {
                    builder.append('\b');
                } else if (ch == 'f') {
                    builder.append('\f');
                } else if (ch == 'u') {

                    StringBuilder hex = new StringBuilder();

                    // expect 4 digits
                    if (i + 4 > input.length()) {
                        throw new RuntimeException("Not enough unicode digits! ");
                    }
                    for (char x : input.substring(i, i + 4).toCharArray()) {
                        if (!Character.isLetterOrDigit(x)) {
                            throw new RuntimeException("Bad character in unicode escape.");
                        }
                        hex.append(Character.toLowerCase(x));
                    }
                    i += 4; // consume those four digits.

                    int code = Integer.parseInt(hex.toString(), 16);
                    builder.append((char) code);
                } else {
                    throw new RuntimeException("Illegal escape sequence: \\" + ch);
                }
            } else { // it's not a backslash, or it's the last character.
                builder.append(delimiter);
            }
        }
        return builder.toString();
    }
}
