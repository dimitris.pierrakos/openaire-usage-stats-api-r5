package eu.dnetlib.usagestats.services;

import eu.dnetlib.usagestats.portal.CountryRepositories;
import eu.dnetlib.usagestats.portal.CountryUsageStats;
import eu.dnetlib.usagestats.portal.CountryUsageStatsAll;
import eu.dnetlib.usagestats.portal.MonthlyUsageStats;
import eu.dnetlib.usagestats.portal.TotalStats;
import eu.dnetlib.usagestats.portal.TotalStatsReposViewsDownloads;
import eu.dnetlib.usagestats.portal.UsageStats;
import java.util.List;

public interface UsageStatsService {
    UsageStats getDatasourceClicks(String id);
    UsageStats getProjectClicks(String id);
    UsageStats getResultClicks(String id);
    UsageStats getResultClicksByPID(String resultPID);
    //UsageStats getOrganizationClicks(String id);
    TotalStats getTotalStats();
    List<MonthlyUsageStats> getMonthlyUsageStats();
    List<MonthlyUsageStats> getMonthlyUsageStatsForRepo(String id);
    CountryUsageStatsAll getCountryUsageStatsAll();
    CountryUsageStats getCountryUsageStats(String country);
    List<CountryRepositories> getCountryRepositories();
    TotalStatsReposViewsDownloads getTotalStatsReposViewsDownloads();
}
